<?php
/*
Plugin Name: Bulletini
Plugin URI: http://www.itmooti.com
Description: Send Tailored WordPress Content To Your ONTRAPORT Contacts
Version: 1.0.8
Author: ITMOOTI
Author URI: http://www.itmooti.com
*/


//Dependent Classes
require_once('inc/class.plugin.php');
require_once('inc/class.ontraport.php');
require_once('inc/class.settings.php');
require_once('inc/class.bulletini_updater.php');

//Main Class
class Bulletini{
	//Private Instances for other classes
	private $ontraport, $settings, $plugin_settings;
	
	//Constructor Function
	public function __construct(){
		
		//Create instances for dependent classes.
		$this->plugin_settings=new bulletini_plugin();
		$this->settings=new bulletini_settings();
		//Check for Updates
		if ( is_admin() ) {
			new bulletini_updater( __FILE__, 'it-mooti', "bulletini" );
		}
		
		//Adding WordPress init() function hook.
		add_action( 'init', array( $this, 'init' ) );
		
		//Register Feed URL to sync single user by providing email address as a GET variable
		add_action('sync_userdata_hook', array($this, 'sync_userdata'));
	}
	public function upgrade_install(){
		$baseUrl = 'https://bitbucket.org//$repository/get/master.zip';
		
		// store the zip file temporary
		$zipFile = 'full-' . time() . '-' . rand(0, 100);
		$zipLocation = $CONFIG['commitsFolder'] . (substr($CONFIG['commitsFolder'], -1) == DIRECTORY_SEPARATOR ? '' : DIRECTORY_SEPARATOR);
	
		
		$result = getFileContents($baseUrl . $repoUrl . $branchUrl, $zipLocation . $zipFile);
	}
	
	//WordPress init() function hook.
	public function init(){
		$app_id=bulletini_option('app_id');
		$api_key=bulletini_option('api_key');
		$this->ontraport=new bulletini_ontraport($app_id, $api_key);
		add_action( 'wp_ajax_bulletini_save_template_settings', array($this, 'save_template_settings'));
		add_feed('sync_userdata', array($this, 'sync_userdata_init'));
		$this->sync_engine();
		if(isset($_POST["bulletini_rehash"])){
			if(wp_verify_nonce( $_POST['bulletini_settings_meta_box_nonce'], 'bulletini_settings_meta_box' ) && current_user_can( 'manage_options')) {
				$this->ontraport=new bulletini_ontraport($app_id, $api_key);
				$this->set_op_data();
			}
		}
		if((!empty($app_id) && !empty($api_key) && (!is_array($this->settings->get_op_contact_fields()) || count($this->settings->get_op_contact_fields())==0))){
			if(current_user_can( 'manage_options')) {
				$this->ontraport=new bulletini_ontraport($app_id, $api_key);
				$this->set_op_data();
			}
		}
		if(isset($_POST["bulletini_fieldssetup"])){
			if (isset( $_POST['bulletini_settings_meta_box_nonce'] ) && wp_verify_nonce( $_POST['bulletini_settings_meta_box_nonce'], 'bulletini_settings_meta_box' ) && current_user_can( 'manage_options')) {
				$this->create_and_setup_fields();
			}
		}
		if(isset($_POST["bulletini_fieldsremove"])){
			if (isset( $_POST['bulletini_settings_meta_box_nonce'] ) && wp_verify_nonce( $_POST['bulletini_settings_meta_box_nonce'], 'bulletini_settings_meta_box' ) && current_user_can( 'manage_options')) {
				$this->remove_fields();
			}
		}
		if(isset($_GET["bulletini_install_template"])){
			if(isset($_GET['template']) && isset($_GET['bulletini_install_template_nonce']) && wp_verify_nonce($_GET['bulletini_install_template_nonce'], 'install-template_'.sanitize_title($_GET['template'])) && current_user_can( 'manage_options') ) {
				$this->install_template();
			}
		}
		if(isset($_GET["bulletini_uninstall_template"])){
			if(isset($_GET['template']) && isset($_GET['bulletini_uninstall_template_nonce']) && wp_verify_nonce($_GET['bulletini_uninstall_template_nonce'], 'uninstall-template_'.sanitize_title($_GET['template'])) && current_user_can( 'manage_options') ) {
				$this->uninstall_template();
			}
		}
		if(isset($_POST["bulletini_custom_interests"]) && isset($_POST["submit"])){
			if (isset( $_POST['bulletini_settings_meta_box_nonce'] ) && wp_verify_nonce( $_POST['bulletini_settings_meta_box_nonce'], 'bulletini_settings_meta_box' ) && current_user_can( 'manage_options')) {
				$this->update_interests_field();
			}
		}
		if(isset($_POST["bulletini_ondemandscript"])){
			if (isset( $_POST['bulletini_settings_meta_box_nonce'] ) && wp_verify_nonce( $_POST['bulletini_settings_meta_box_nonce'], 'bulletini_settings_meta_box' ) && current_user_can( 'manage_options')) {
				update_option('bulletini_contact_tag', sanitize_text_field($_POST["bulletini_contact_tag"]));
				$this->sync_userdata();
			}
		}
	}
	
	//Template Functions
	public function install_template(){
		if(isset($_GET["template"])){
			$template=sanitize_title($_GET["template"]);
			$bulletini_template_message_id=bulletini_option('template_'.$template.'_message_id');
			if(empty($bulletini_template_message_id)){
				if($this->ontraport->is_valid()){
					$template_directory=plugin_dir_path( __FILE__ ).'templates/'.$template;
					if(is_dir($template_directory)){
						$template_content=$this->get_template_content($template);
						//echo $template_content; die;
						$config_data=array();
						$config_file=$template_directory."/config.xml";
						if(file_exists($config_file)){
							$config_data=simplexml_load_string(file_get_contents($config_file));
							if(!$config_data){
								$config_data=array();
							}
							else{
								$config_data=(array)$config_data;
							}
						}
						$template_name="Bulletini - ".(isset($config_data["name"])?$config_data["name"]:$template);
						$message=$this->ontraport->Request('message', array("objectID"=>"7", "type"=>"e-mail", "alias"=>$template_name, "name"=>$template_name, "subject"=>'[Owner//Business Name] Newsletter',  "message_body"=>$template_content), true, "post");
						if(isset($message->id)){
							update_option('bulletini_template_'.$template.'_message_id', $message->id);
							$this->add_notice('Template Installed Successfully. Click Edit Template button to edit it.', 'message', 'Template'.$template);
						}
						else{
							$this->add_notice('Error in creating message. Please Try Again or Contact Support', 'error', 'Template'.$template);
						}
					}
					else{
						$this->add_notice('Template Directory missing. Reinstall the plugin.', 'error', 'Template'.$template);
					}
				}
				else{
					$this->add_notice('APP ID or API Key is not valid. Please confirm and submit again.', 'error', 'Template'.$template);
				}
			}
			else{
				$this->add_notice('Template is already installed. Click Edit Template button to edit it within your ONTRAPORT account.', 'error', 'Template'.$template);
			}
		}
		else{
			$this->add_notice('Template name is empty. Try again.', 'error', 'Template'.$template);
		}
	}
	public function uninstall_template(){
		if(isset($_GET["template"])){
			$template=sanitize_title($_GET["template"]);
			$bulletini_template_message_id=bulletini_option('template_'.$template.'_message_id');
			if(!empty($bulletini_template_message_id)){
				$message=$this->ontraport->Request('object', array("objectID"=>"7", "id"=>$bulletini_template_message_id), false, "delete");
				delete_option('bulletini_template_'.$template.'_message_id');
				$this->add_notice('Template uninstalled successfully.', 'message', 'Template'.$template);
			}
			else{
				$this->add_notice('Template not installed.', 'error', 'Template'.$template);
			}
		}
	}
	public function get_template_content($template){
		$template_directory=plugin_dir_path( __FILE__ ).'templates/'.$template;
		$config_data=array();
		$config_file=$template_directory."/config.xml";
		if(file_exists($config_file)){
			$config_data=simplexml_load_string(file_get_contents($config_file));
			if(!$config_data){
				$config_data=array();
			}
			else{
				$config_data=(array)$config_data;
			}
		}
		$template_content=file_get_contents($template_directory."/template.html");
		preg_match_all("/\[Condition[^\]]*\]/", $template_content, $merge_fields);
		foreach($merge_fields[0] as $merge_field){
			$conditional_content=explode($merge_field, $template_content);
			if(isset($conditional_content[1])){
				$conditional_content=explode(str_replace('[Condition', '[/Condition', $merge_field), $conditional_content[1]);
				$conditional_content=$conditional_content[0];
			}
			else
				$conditional_content="";
			$is_empty=true;
			if(!empty($conditional_content)){
				$variables=explode('|', trim(str_replace(array('[Condition:', ']'), array('', ''), $merge_field)));
				foreach($variables as $variable){
					$field_input_key='bulletini_'.$template."_".str_replace("-", "_", sanitize_title($variable));
					$field_value=get_option($field_input_key);
					if(!empty($field_value)){
						$is_empty=false;
						break;
					}
				}
			}
			$replace_with='';
			if(!$is_empty){
				$replace_with=$conditional_content;
			}
			$template_content=str_replace($merge_field.$conditional_content.str_replace('[Condition', '[/Condition', $merge_field),$replace_with, $template_content);
		}
		preg_match_all("/\[Config[^\]]*\]/", $template_content, $merge_fields);
		if(isset($config_data["configuration"])){
			foreach($config_data["configuration"] as $field_type=>$field_key){
				$field_input_key='bulletini_'.$template."_".str_replace("-", "_", sanitize_title($field_key));
				$field_value=get_option($field_input_key);
				switch($field_type){
					case "text":break;
					case "textarea": break;
					case "image":
						$img_src = wp_get_attachment_image_src( $field_value, 'full' );
						$field_value=$img_src[0];
					break;
					case "color":
						$bar_image=plugin_dir_path( __FILE__ ).'templates/'.$template."/images/bar.png";
						if(file_exists($bar_image) && !empty($field_value)){
							$img = imagecreatetruecolor(1,4);
							$color=$this->hex2rgb($field_value);
							imagesetpixel($img, 0, 0, imagecolorallocate($img, 255, 255, 255));
							imagesetpixel($img, 0, 1, imagecolorallocate($img, $color[0], $color[1], $color[2]));
							imagesetpixel($img, 0, 2, imagecolorallocate($img, $color[0], $color[1], $color[2]));
							imagesetpixel($img, 0, 3, imagecolorallocate($img, 255, 255, 255));
							imagepng($img, $bar_image);
						}
					break;
				}
				$template_content=str_replace(array('[Config:'.$field_key.']', '[Config: '.$field_key.']'), array($field_value, $field_value), $template_content);
			}
		}
		$template_content=str_replace(array(
			'[Option:Template URL]',
			'[Option: Template URL]'
		),
		array(
			plugins_url("/templates/".$template, __FILE__),
			plugins_url("/templates/".$template, __FILE__)
		),
		$template_content);
		preg_match_all("/\[[^\]]*\]/", $template_content, $merge_fields);
		$field_map=$this->array_flatten($this->settings->get_fields_map());
		if(isset($merge_fields[0]) && is_array($merge_fields[0])){
			foreach($merge_fields[0] as $merge_field){
				$field_map_key=array_search(str_replace(array(" ", "description", '[', ']'), array("_", "desc", '', ''), strtolower($merge_field)), $field_map);
				if($field_map_key!==false){
					$merge_field_alias=$this->ontraport->getFieldAlias(bulletini_option($field_map_key));
					if(!empty($merge_field_alias)){
						$merge_field_alias='['.$merge_field_alias.']';
					}
					$template_content=str_replace($merge_field, $merge_field_alias, $template_content);
				}
			}
		}
		return $template_content;
	}
	public function save_template_settings(){
		$return=0;
		if(current_user_can('manage_options')){
			if(isset($_POST["bulletini_template"]) && !empty($_POST["bulletini_template"])){
				$template=sanitize_title($_POST["bulletini_template"]);
				foreach($_POST as $field_key=>$field_value){
					if(strpos($field_key, 'bulletini_'.$template."_")!==false){
						update_option($field_key, esc_attr(sanitize_text_field($field_value)));
						$return=1;
					}
				}
				$bulletini_template_message_id=bulletini_option('template_'.$template.'_message_id');
				if(!empty($bulletini_template_message_id)){
					$template_content=$this->get_template_content($template);
					$message=$this->ontraport->Request('message', array("objectID"=>"7", "id"=>$bulletini_template_message_id, "message_body"=>$template_content), true, "put");
				}
			}
		}
		echo $return;
		wp_die();
	}
	
	//Interest Field Functions
	public function get_all_interests(){
		$interests=array();
		$interest_field=bulletini_option('bulletini_interests');
		$contact_fields=$this->ontraport->getContactFields(1);
		if(!empty($interest_field) && isset($contact_fields->$interest_field->options)){
			foreach($contact_fields->$interest_field->options as $interest){
				$interests[]=$interest;
			}
		}
		return $interests;
	}
	public function update_interests_field(){
		$current_interests=$this->get_all_interests();
		$new_interests=$this->settings->get_custom_interests();
		$interests_to_remove=array_diff($current_interests, $new_interests);
		$interests_to_add=array_diff($new_interests, $current_interests);
		if(count($interests_to_remove)>0){
			$option_str='';
			foreach($interests_to_remove as $interest){
				$option_str.='<option>'.$interest.'</option>';
			}
			$parameters=array(
				"reqType"=>"remove_dropdown",
				"data"=>'<data>
					<field name="Bulletini Interests">
						'.$option_str.'
					</field>
				</data>'
			);
			var_dump($this->ontraport->Request("cdata", $parameters));
		}
		if(count($interests_to_add)>0){
			$option_str='';
			foreach($interests_to_add as $interest){
				$option_str.='<option>'.$interest.'</option>';
			}
			$parameters=array(
				"reqType"=>"add_dropdown",
				"data"=>'<data>
					<field name="Bulletini Interests">
						'.$option_str.'
					</field>
				</data>'
			);
			$this->ontraport->Request("cdata", $parameters);
		}
		if(count($interests_to_remove)>0 || count($interests_to_add)>0){
			$contact_fields=$this->ontraport->getContactFields(1);
			$this->settings->set_op_contact_fields($contact_fields);	
		}
	}
	
	//Fields Functions
	public function create_and_setup_fields(){
		set_time_limit(300);
		if(isset($_POST['bulletini_no_of_fields2'])){
			$bulletini_no_of_fields=absint(sanitize_text_field($_POST['bulletini_no_of_fields2']));
			update_option('bulletini_no_of_fields', $bulletini_no_of_fields);
		}
		else{
			$bulletini_no_of_fields=bulletini_option('no_of_fields');
		}
		$interests='';
		$taxonomy=bulletini_option("taxonomy");
		$bulletini_add_interests=bulletini_option('add_interests');
		if($bulletini_add_interests==1){
			$new_interests=$this->settings->get_custom_interests();
			foreach($new_interests as $new_interest){
				$interests.='<option>'.$new_interest.'</option>';
			}
		}
		else{
			$args=array("taxonomy"=>$taxonomy, "hide_empty"=>false);
			$terms=get_terms($args);
			foreach($terms as $term){
				$interests.='<option>'.$term->name.'</option>';
			}
		}
		
		//Find All Fields to be added
		$field_map=$this->array_flatten($this->settings->get_fields_map());
		$fields_to_add=$field_map;
		$contact_fields=(array)$this->ontraport->getContactFields(1);
		$i=0;
		foreach($contact_fields as $field_key=>$field){
			$contact_field_key=str_replace(array(" ", "description"), array("_", "desc"), strtolower($field->alias));
			$field_map_key=array_search($contact_field_key, $field_map);
			if($field_map_key!==false){
				update_option("bulletini_".$field_map_key, $field_key);
				unset($fields_to_add[$field_map_key]);
				$i++;
			}
		}
		if(count($fields_to_add)>0){
			$field_setup_str='';
			if(isset($fields_to_add["bulletini_interests"])){
				$field_setup_str.='<field name="Bulletini Interests" type="list">
					'.$interests.'
				</field>';
			}
			if(isset($fields_to_add["past_post"])){
				$field_setup_str.='<field name="Past Post" type="longtext"/>';						
			}
			if(!empty($field_setup_str)){
				$parameters=array(
					"reqType"=>"add_section",
					"data"=>'<data>
						<Group_Tag name="'.$this->app_name().' - Settings">
							'.$field_setup_str.'
						</Group_Tag>
					</data>'
				);
				$this->ontraport->Request("cdata", $parameters);
			}
			$field_setup_str='';
			foreach(array("Title", "Image", "Description", "Author", "Link") as $article_field_key){
				if(isset($fields_to_add["articles_a1_".str_replace("description", "desc", strtolower($article_field_key))])){
					$field_setup_str.='<field name="A1 '.$article_field_key.'" type="longtext"/>';
				}	
			}
			if(!empty($field_setup_str)){
				$parameters=array(
					"reqType"=>"add_section",
					"data"=>'<data>
						<Group_Tag name="'.$this->app_name().' - A: Featured Article">
							'.$field_setup_str.'
						</Group_Tag>
					</data>'
				);
				$this->ontraport->Request("cdata", $parameters);
			}
			$field_setup_str='';
			for($article_no=1; $article_no<=3; $article_no++){
				foreach(array("Title", "Image", "Description", "Author", "Link") as $article_field_key){
					if(isset($fields_to_add['articles_b'.$article_no.'_'.str_replace("description", "desc", strtolower($article_field_key))])){
						$field_setup_str.='<field name="B'.$article_no.' '.$article_field_key.'" type="longtext"/>';
					}	
				}
			}
			if(!empty($field_setup_str)){
				$parameters=array(
					"reqType"=>"add_section",
					"data"=>'<data>
						<Group_Tag name="'.$this->app_name().' - B: Dynamic Content">
							'.$field_setup_str.'
						</Group_Tag>
					</data>'
				);
				$this->ontraport->Request("cdata", $parameters);
			}
			$field_setup_str='';
			for($article_no=1; $article_no<=3; $article_no++){
				foreach(array("Title", "Image", "Description", "Author", "Link") as $article_field_key){
					if(isset($fields_to_add['articles_c'.$article_no.'_'.str_replace("description", "desc", strtolower($article_field_key))])){
						$field_setup_str.='<field name="C'.$article_no.' '.$article_field_key.'" type="longtext"/>';
					}	
				}
			}
			if(!empty($field_setup_str)){
				$parameters=array(
					"reqType"=>"add_section",
					"data"=>'<data>
						<Group_Tag name="'.$this->app_name().' - C: Dynamic Content">
							'.$field_setup_str.'
						</Group_Tag>
					</data>'
				);
				$this->ontraport->Request("cdata", $parameters);
			}
			$field_map=$this->array_flatten($this->settings->get_fields_map());
			$contact_fields=(array)$this->ontraport->getContactFields(1);
			$i=0;
			foreach($contact_fields as $field_key=>$field){
				$field_map_key=array_search(str_replace(array(" ", "description"), array("_", "desc"), strtolower($field->alias)), $field_map);
				if($field_map_key!==false){
					update_option("bulletini_".$field_map_key, $field_key);
					$i++;
				}
			}
		}
		$this->add_notice('Fields has been created and setup successfully.', 'message', 'fieldsadd');
	}
	public function remove_fields(){
		set_time_limit(300);
		$bulletini_no_of_fields=bulletini_option('no_of_fields');
		$parameters=array(
			"reqType"=>"remove_field",
			"data"=>'<data>
				<field name="A1 Title"/>
				<field name="A1 Image"/>
				<field name="A1 Description"/>
				<field name="A1 Author"/>
				<field name="A1 Link"/>
				<field name="B1 Title"/>
				<field name="B1 Image"/>
				<field name="B1 Description"/>
				<field name="B1 Author"/>
				<field name="B1 Link"/>
				<field name="B2 Title"/>
				<field name="B2 Image"/>
				<field name="B2 Description"/>
				<field name="B2 Author"/>
				<field name="B2 Link"/>
				'.($bulletini_no_of_fields!=3?'<field name="B3 Link"/>
					<field name="B3 Title"/>
					<field name="B3 Image"/>
					<field name="B3 Description"/>
					<field name="B3 Author"/>
					<field name="B3 Link"/>
					<field name="C1 Title"/>
					<field name="C1 Image"/>
					<field name="C1 Description"/>
					<field name="C1 Author"/>
					<field name="C1 Link"/>
				':'').($bulletini_no_of_fields!=5?'
					<field name="C2 Title"/>
					<field name="C2 Image"/>
					<field name="C2 Description"/>
					<field name="C2 Author"/>
					<field name="C2 Link"/>
				':'').($bulletini_no_of_fields==7?'
					<field name="C3 Title"/>
					<field name="C3 Image"/>
					<field name="C3 Description"/>
					<field name="C3 Author"/>
					<field name="C3 Link"/>
				':'').'
			</data>'
		);
		$this->ontraport->Request("cdata", $parameters);
		$field_map=$this->array_flatten($this->settings->get_fields_map());
		$i=0;
		foreach($field_map as $field_map_key=>$field_map_value){
			update_option("bulletini_".$field_map_key, '');
		}
		$this->add_notice('All the fields have been removed successfully.', 'message', 'fieldsremove');
	}
	
	//Scheduler Functions
	public function set_op_data(){
		$tags=$this->ontraport->getTags();
		$this->settings->set_op_tags($tags);
		$contact_fields=$this->ontraport->getContactFields();
		$this->settings->set_op_contact_fields($contact_fields);
	}
	public function sync_engine(){
		//wp_clear_scheduled_hook('sync_userdata_hook');
		$bulletini_cron_schedule=bulletini_option('cron_schedule');
		if(in_array($bulletini_cron_schedule, array("daily","weekly","monthly"))){
			//echo $bulletini_cron_schedule;
			$next_run=bulletini_next_run($bulletini_cron_schedule);
			if($next_run){
				//echo current_time('timestamp')."<=".$next_run."next=".date("Y-m-d H:i:s", $next_run);
				if(current_time('timestamp')>=$next_run){
					$timestamp=wp_next_scheduled('sync_userdata_hook');
					//echo "ts=".$timestamp.date("Y-m-d H:i:s", $timestamp);
					if($timestamp===false){
						//echo "tes";
						wp_schedule_single_event(time(), 'sync_userdata_hook');
					}
				}
			}
		}
		//echo bulletini_option('cron_schedule_weekly_last_run');
	}
	public function sync_userdata_init(){
		$this->sync_userdata();
		if(isset($_GET["email"])){
			$this->sync_userdata();
		}
	}
	public function sync_userdata(){
		set_time_limit(7200);
		if($this->ontraport->is_valid()){
			if(isset($_POST["bulletini_ondemandscript"])){
				$sync_log_action="ondemand";
			}
			else{
				$sync_log_action="scheduled";
			}
			//Post Type from where articles will be extracted.
			$post_type=bulletini_option('post_type', false, 'past_post');
			$no_of_fields=bulletini_option('no_of_fields', false, '7');
			$bulletini_no_of_fields_sync=bulletini_option('no_of_fields_sync');
			if( !empty( $bulletini_no_of_fields_sync ) ) {
				$no_of_fields = $bulletini_no_of_fields_sync;
			}
			//Get Contact based on search.
			if(!isset($_GET["email"])){ //if True then sync all the contacts.
				//User defined Tag to filter contacts from.
				$bulletini_contact_tag=bulletini_option('contact_tag');
				
				//Loop to get all the contacts as maximum records limit is 50
				$users=array();
				try{
					$start=0;
					do{
						$parameters=array("objectID"=>"138", "start"=>$start);
						if(!empty($bulletini_contact_tag)){
							$parameters["condition"]="tag_id='".$bulletini_contact_tag."'";
						}
						$users_loop=$this->ontraport->Request('objects', $parameters);
						if(is_array($users_loop)){
							$users=array_merge($users, $users_loop);
							$start+=50;
						}
						else{
							$users_loop=array();
						}	
					} while(count($users_loop)==50);
				}catch(Exception $e){
					die("Error in Fetching contact records");
				}
				
				//Loop through all the contact IDs to get full info.
				$contacts=array();
				foreach($users as $user){
					$contacts[]=$this->ontraport->getContactByID($user->object_id);
				}
			}
			else{ //Else just get particular contact.
				$contacts[]=$this->ontraport->findContactByEmail($_GET["email"]);
			}
			if($contacts){
				$cnt=1;
				$past_post_field=bulletini_option('past_post');
				$interests_field=bulletini_option('bulletini_interests');
				$add_featured_field=bulletini_option('add_featured_field');
				if($add_featured_field=="1"){
					$args=array(
						"post_type"=>$post_type,
						"posts_per_page"=>'1',
						"meta_query"=>array(
							array(
								"key"=>"bulletini_is_featured",
								"value"=>'1',
							)
						)
					);
					$articles=get_posts($args);
					if(count($articles)>0){
						$featured_article=$articles[0];
					}
				}
				$sync_count=$this->settings->get_sync_count();
				foreach($contacts as $contact){
					if($sync_count>0){
						if(is_object($contact)){
							$interests=array();
							$exclude_posts=array();
							$article=array();
							if(!empty($past_post_field) && isset($contact->$past_post_field) && !empty($contact->$past_post_field)){
								$exclude_posts=explode(",", $contact->$past_post_field);
							}
							$contact_fields=$this->ontraport->getContactFields(1);
							if(!empty($interests_field) && isset($contact->$interests_field) && !empty($contact->$interests_field)){
								$interests_str=$contact->$interests_field;
								if(!empty($interests_str)){
									foreach(explode('*/*', trim($interests_str, '*/*')) as $interest){
										$interests[]=$this->ontraport->getDropOption($interests_field, $interest);
									}
								}
							}
							$cnt_articles=1;
							if(isset($featured_article)){
								$article["a1"]=$this->populate_post($featured_article, 'a1');
								if(!in_array($featured_article->ID, $exclude_posts)){
									$exclude_posts[]=$featured_article->ID;
								}
								$cnt_articles++;
							}
							$taxonomy=bulletini_option("taxonomy");
							$bulletini_add_interests=bulletini_option('add_interests');
							if(!empty($interest)){
								$args=array("taxonomy"=>$taxonomy, "hide_empty"=>false);
								if($bulletini_add_interests==1){
									$args["meta_query"]=array(
										array(
											"key"=>"bulletini_interests",
											"value"=>$interests,
											"compare"=>'IN'
										)
									);
								}
								else{
									$args["name"]=$interests;
								}
								$terms=get_terms($args);
							}
							if(count($article)<$no_of_fields){
								$args=array(
									"post_type"=>$post_type,
									"posts_per_page"=>$no_of_fields-count($article),
									"exclude"=>$exclude_posts
								);
								if(isset($terms) && count($terms)>0){
									$term_ids=array();
									foreach($terms as $term){
										$term_ids[]=$term->term_id;
									}
									$args["tax_query"]=array(
										array(
											"taxonomy"=>$taxonomy,
											"field"=>"term_id",
											"terms"=>$term_ids
										)
									);
								}
								$articles=get_posts($args);
								if(count($articles)>0){
									foreach($articles as $post){
										switch($cnt_articles){
											case 1: $article_number="a1"; break;
											case 2:case 3:case 4: $article_number="b".($cnt_articles-1); break;
											default: $article_number="c".($cnt_articles-4); break;
										}
										$article[$article_number]=$this->populate_post($post, $article_number);
										$exclude_posts[]=$post->ID;
										$cnt_articles++;
									}
								}	
							}
						}
						if(count($article)<$no_of_fields){
							$remaining=$no_of_fields-count($article);
							$args=array(
								"post_type"=>$post_type,
								"posts_per_page"=>$remaining,
								"exclude"=>$exclude_posts
							);
							$articles=get_posts($args);
							foreach($articles as $post){
								switch($cnt_articles){
									case 1: $article_number="a1"; break;
									case 2:case 3:case 4: $article_number="b".($cnt_articles-1); break;
									default: $article_number="c".($cnt_articles-4); break;
								}
								$article[$article_number]=$this->populate_post($post, $article_number);
								$exclude_posts[]=$post->ID;
								$cnt_articles++;
							}
						}
						if(count($article)<$no_of_fields){
							for($i=0; $i<$no_of_fields-count($article); $i++){
								switch($cnt_articles){
									case 1: $article_number="a1"; break;
									case 2:case 3:case 4: $article_number="b".($cnt_articles-1); break;
									default: $article_number="c".($cnt_articles-4); break;
								}
								$article[$article_number]=array(
									"title"=>"",
									"image"=>"",
									"desc"=>"",
									"author"=>"",
									"link"=>"",
								);
								$cnt_articles++;
							}
						}		
						$this->save_newsletter_fields($article, $contact->id, $exclude_posts, $interests);
						$sync_count--;
						$this->settings->set_sync_count($sync_count);
						if(isset($_POST["bulletini_ondemandscript"]) && $cnt>=100){
							$this->set_sync_count($sync_count);
							$this->add_notice('Only 100 contacts can be synchronized on demand.', 'message', 'OnDemandScript');
							break;
						}
						$cnt++;
					}
					else{
						$log_text=($cnt-1).' contacts successfully synchronized. Your monthly contact subscription is over.';
						$this->add_notice($log_text, 'information', 'OnDemandScript');
						$this->sync_log("limit_over", $log_text, $sync_log_action);
						$sync_over_limit=true;
						$this->set_sync_count($sync_count);
						break;
					}
				}
				if(!isset($sync_over_limit)){
					$this->set_sync_count($sync_count);
					$log_text=count($contacts).' contacts successfully synchronized.';
					$this->add_notice($log_text, 'message', 'OnDemandScript');
					$this->sync_log("success", $log_text, $sync_log_action);
					//$this->settings->print_notice('OnDemandScript');
					//die;
				}
			}
			else{
				$log_text='No contacts to be synchronized.';
				$this->add_notice($log_text, 'error', 'OnDemandScript');
				$this->sync_log("error", $log_text, $sync_log_action);
			}
		}
		else{
			$log_text='APP ID or API Key is not valid. Please confirm and try again.';
			$this->add_notice($log_text, 'error', 'OnDemandScript');
			$this->sync_log("error", $log_text, $sync_log_action);
		}
		if(!isset($_POST["bulletini_ondemandscript"])){
			$bulletini_cron_schedule=bulletini_option('cron_schedule');
			if(in_array($bulletini_cron_schedule, array("daily","weekly","monthly"))){
				update_option('bulletini_cron_schedule_'.$bulletini_cron_schedule.'_last_run', current_time('timestamp'));
			}
		}
	}
	public function save_newsletter_fields($articles, $contact_id, $exclude_posts){
		$fields_map=$this->settings->get_fields_map();
		$fields=array();
		//Articles Fields
		$article_fields=$fields_map["articles"];
		foreach($articles as $k=>$article){
			foreach($article as $k1=>$value){
				$fields[$article_fields[$k][$k1]]=$value;
			}
		}
		$fields[$fields_map["past_post"]]=implode(",",$exclude_posts);
		$result=$this->ontraport->saveUserData($contact_id, $fields);
		if(is_string($result) && strpos($result, 'API rate limit exceeded')!==false){
			sleep(60);
			$this->save_newsletter_fields($articles, $contact_id, $exclude_posts);
		}
	}
	public function set_sync_count($sync_count){
		$license_key=get_option('bulletini_license_key', "");
		$url="http://app.itmooti.com/wp-plugins/oap-utm/api-new.php";
		$request= "sync_count";
		$postargs = "plugin=bulletini&domain=".urlencode($_SERVER['HTTP_HOST'])."&license_key=".urlencode($license_key)."&request=".urlencode($request)."&sync_count=".$sync_count;
		$session = curl_init($url);
		curl_setopt ($session, CURLOPT_POST, true);
		curl_setopt ($session, CURLOPT_POSTFIELDS, $postargs);
		curl_setopt($session, CURLOPT_HEADER, false);
		curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($session, CURLOPT_CONNECTTIMEOUT ,3); 
		curl_setopt($session, CURLOPT_TIMEOUT, 3);
		$response = json_decode(curl_exec($session));
		curl_close($session);
	}
	public function sync_log($type, $log_text, $sync_log_action){
		$this->settings->add_sync_log($type, $log_text, $sync_log_action);
	}
	
	//Utility Functions
	public function app_name(){
		return "Bulletini";
	}
	public function populate_post($post, $position){
		$post_data=array();
		foreach(array(
			"title"=>"Title",
			"image"=>"Photo",
			"desc"=>"Description",
			"author"=>"Author",
			"link"=>"Link",
		) as $k=>$v){
			$bulletini_field=bulletini_option($k.'_field');
			switch($bulletini_field){
				case "post_title":
					$bulletini_field_value=$post->post_title;
				break;
				case "post_content":
					$bulletini_field_value = wp_trim_words( strip_tags( $post->post_content ), 35 );
				break;
				case "post_thumbnail":
					$image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large');
					$bulletini_field_value=$image[0];
				break;
				case "post_author":
					$author=get_userdata($post->post_author);
					$bulletini_field_value=$author->display_name;
				break;
				case "permalink":
					$bulletini_field_value=get_permalink($post->ID);
				break;
				default:
					$bulletini_field_value=get_post_meta($post->ID, $bulletini_field, true);
			}
			if($k=="link"){
				$bulletini_enable_utms=bulletini_option('enable_utms');
				if($bulletini_enable_utms==1){
					$utms=array(
						"utm_source"=>strtolower($this->app_name()),
					 	"utm_medium"=>"email",
						"utm_content"=>$post->ID,
						"utm_campaign"=>current_time("ymd"),
						"utm_term"=>$position
					);
					$bulletini_field_value=add_query_arg($utms, $bulletini_field_value);
				}
			}
			$post_data[$k]=$bulletini_field_value;
		}
		return $post_data;
	}
	public function getTopicCatIDs($topics){
		$topic_cat_ids=array();
		$cats=get_categories();
		if($cats){
			foreach($cats as $cat){
				$has_topics=false;
				$cat_topics=get_field('topics', 'product-cat_'.$cat->term_id);
				if(is_array($cat_topics) && count($cat_topics)>0){
					foreach($cat_topics as $cat_topic){
						if(in_array($cat_topic, $topics)){
							$has_topics=true;
							break;
						}
					}
				}
				if($has_topics){
					$topic_cat_ids[]=$cat->term_id;
				}
			}
		}
		return $topic_cat_ids;
	}
	public function hex2rgb($color) {
		$default = array(0,0,0);
		if(empty($color)){
			return $default; 
		}
		if ($color[0] == '#' ) {
			$color = substr( $color, 1 );
		}
		if (strlen($color) == 6) {
				$hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
		} elseif ( strlen( $color ) == 3 ) {
			$hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
		} else {
			return $default;
		}
		$output =  array_map('hexdec', $hex);	
		return $output;
	}
	public function array_flatten($array) { 
		if (!is_array($array)) { 
			return FALSE; 
		}
		$result = array(); 
		foreach ($array as $key => $value) { 
			if (is_array($value)) {
				$value=array_combine(
					array_map(create_function('$k', 'return "'.$key.'_".$k;'), array_keys($value))
					, $value
				);
				$result = array_merge($result, $this->array_flatten($value)); 
			} 
			else { 
				$result[$key]=str_replace("articles_", "", $key); 
			} 
		} 
		return $result; 
	}
	
	//Bridge Functions
	public function add_notice($text, $type='message', $location='general'){
		$this->settings->add_notice($text, $type, $location);
	}
	public function option($option_name, $apply_the_content=false, $default_value=""){
		return $this->settings->option($option_name, $apply_the_content, $default_value);
	}
	public function is_authenticated(){
		return $this->plugin_settings->is_authenticated();
	}
	public function is_valid(){
		return $this->ontraport->is_valid();
	}
	
}
//Initialization
$Bulletini=new Bulletini();

//Common Functions
function bulletini_option($option_name, $apply_the_content=false, $default_value=""){
	global $Bulletini;
	return $Bulletini->option($option_name, $apply_the_content, $default_value);
}
function bulletini_authenticated(){
	global $Bulletini;
	if(isset($Bulletini)){
		return $Bulletini->is_authenticated();
	}
}
function bulletini_connected(){
	global $Bulletini;
	if(isset($Bulletini)){
		return $Bulletini->is_valid();
	}
}
function bulletini_add_notice($text, $type='message', $location='general'){
	global $Bulletini;
	if(isset($Bulletini)){
		$Bulletini->add_notice($text, $type, $location);
	}
}
function bulletini_time_string($i){
	if($i==0){
		$time_string="12:00 AM";
	}
	else if($i<10){
		$time_string="0".$i.":00 AM";
	}
	else if($i<12){
		$time_string=$i.":00 AM";
	}
	else if($i==12){
		$time_string="12:00 PM";
	}
	else{
		$i=$i-12;
		if($i<10){
			$i="0".$i;
		}
		$time_string=$i.":00 PM";
	}
	return $time_string;
}
function bulletini_next_run($type, $last_run=0){
	$start_date=bulletini_option('cron_schedule_'.$type.'_start_date');
	if($last_run==0) $last_run=bulletini_option('cron_schedule_'.$type.'_last_run');
	$time=bulletini_option('cron_schedule_'.$type.'_time');
	if(!empty($start_date)){
		$start_ts=strtotime($start_date);
		if($last_run<$start_ts){
			$last_run=0;
		}
		
		if($type=="daily"){
			if($last_run>0 && $last_run>=(strtotime(date("Y-m-d"))-24*3600*$daily_days)){
				$daily_days=bulletini_option('cron_schedule_daily_days');
				$next_run=$last_run+(24*3600*$daily_days);
				$next_run=strtotime(date("Y-m-d", $next_run));
			}
			else{
				if($start_ts>=strtotime(date("Y-m-d"))){
					$next_run=$start_ts;
				}
				else{
					$next_run=strtotime(date("Y-m-d"));
				}
			}
		}
		else if($type=="weekly"){
			$weekly_weeks=bulletini_option('cron_schedule_weekly_weeks');
			$weekly_days=bulletini_option('cron_schedule_weekly_days');
			if(!is_array($weekly_days)){
				$weekly_days=array();
			}
			$weekly_days_number=array();
			$days_array=array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
			foreach($weekly_days as $weekly_day){
				$day_index=array_search($weekly_day, $days_array);
				if($day_index!==false){
					$weekly_days_number[]=$day_index;
				}
			}
			if(count($weekly_days_number)>0){
				if($last_run>0 && $last_run>=(strtotime("next ".$days_array[$weekly_days_number[count($weekly_days_number)-1]])-7*24*3600*$weekly_weeks)){
					sort($weekly_days_number);
					$last_run_day=date("w", $last_run);
					$day_index=array_search($last_run_day, $weekly_days_number);
					if($day_index===false && $last_run_day<$weekly_days_number[count($weekly_days_number)-1]){
						foreach($weekly_days_number as $day_key=>$day){
							if($day>$last_run_day){
								$day_index=$day_key;
								break;
							}
						}
					}
					if($day_index!==false){
						$next_run=strtotime("next ".$days_array[$weekly_days_number[$day_index]], $last_run);
					}
					else{
						$next_run=strtotime("next ".strtolower($days_array[$weekly_days_number[0]]), $last_run+24*3600);
						$next_run=$next_run+(7*24*3600*($weekly_weeks-1));
					}
					$next_run=strtotime(date("Y-m-d", $next_run));
				}
				else{
					if($start_ts<strtotime(date("Y-m-d"))){
						$start_ts=strtotime(date("Y-m-d"));
					}
					$start_day=date("w", $start_ts);
					$day_index=array_search($start_day, $weekly_days_number);
					if($day_index===false && $start_day<$weekly_days_number[count($weekly_days_number)-1]){
						foreach($weekly_days_number as $day_key=>$day){
							if($day>$start_day){
								$day_index=$day_key;
								break;
							}
						}
					}
					if($day_index!==false && $day_index<=(count($weekly_days_number)-1)){
						$next_run=strtotime("next ".$days_array[$weekly_days_number[$day_index]], $start_ts-24*3600);
					}
					else{
						$next_run=strtotime("next ".$days_array[$weekly_days_number[0]], $start_ts);
					}
				}
			}
		}
		else if($type=="monthly"){
			$monthly_months=bulletini_option('cron_schedule_monthly_months');
			if($last_run>0 && $last_run>=strtotime("-".$monthly_months." months", strtotime(date("Y-m-d")))){
				$next_run=strtotime("+".$monthly_months." months", $start_ts);
				$next_run=strtotime(date("Y-m-d", $next_run));
			}
			else{
				if($start_ts>=strtotime(date("Y-m-d"))){
					$next_run=$start_ts;
				}
				else{
					$next_run=strtotime("+1 months", $start_ts);
				}
			}
		}
		
		if(isset($next_run)){
			if($next_run<strtotime(date("Y-m-d"))){
				$next_run=strtotime(date("Y-m-d"));
			}
			$next_run+=$time*3600;			
		}
		else{
			$next_run=0;
		}
		return $next_run;
	}
	return false;
}