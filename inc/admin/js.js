jQuery(window).load(function(){
	jQuery('.bulletini_selectbox').trigger('chosen:updated');
});
jQuery(document).ready(function(){
	jQuery('.bulletini_selectbox').chosen({"width": "100%"});
	jQuery('.bulletini_template_options_page').fancybox({type: 'inline'});
	jQuery(".bulletini-fields .content-elements").hide();
	jQuery(".bulletini-fields .content-elements:first-child").addClass('current').show();
	jQuery(".left-links li:first-child").addClass("current");
	jQuery(".left-links a").click(function(e){
		e.preventDefault();
		document.location.hash=jQuery(this).attr("href");
		jQuery(".left-links li").removeClass("current");
		jQuery(this).parent().addClass("current");
		jQuery(".bulletini-fields .content-elements.current").removeClass("current").hide();
		jQuery(".bulletini-fields "+jQuery(this).attr("href")).addClass("current").show();
	});
	jQuery(".left-links a.bulletini_external_link").unbind('click');
	jQuery(".bulletini_toggle").change(function(){
		$bulletini_toggle_selector=jQuery(this).data("target");
		if(jQuery(this).is(":checked") && jQuery(this).val()!=""){
			jQuery($bulletini_toggle_selector).slideDown();
		}
		else{
			jQuery($bulletini_toggle_selector).slideUp();
		}
	});
	jQuery(".bulletini_datefield").datepicker();
	jQuery(".bulletini_cron_schedule").change(function(){
		$val=jQuery(this).find("option:selected").val();
		jQuery(".bulletini_cron_schedule_settings").hide();
		jQuery(".bulletini_cron_schedule_settings.bulletini_cron_schedule_"+$val).show();
	}).change();
	jQuery("#expand_options").click(function(){
		if(jQuery(this).hasClass('close')){
			jQuery(this).removeClass("close").addClass("expand");
			jQuery(this).parents(".bulletini-fields").removeClass("expanded").addClass("not-expanded");
			jQuery(this).parents(".bulletini-fields").find(".content-elements").not(".current").hide();
		}
		else{
			jQuery(this).removeClass("expand").addClass("close");
			jQuery(this).parents(".bulletini-fields").removeClass("not-expanded").addClass("expanded");
			jQuery(this).parents(".bulletini-fields").find(".content-elements").show();
		}
	});
	$hash_value=window.location.href.split("#");
	if($hash_value[1]){
		if(jQuery(".left-links a[href='#"+$hash_value[1]+"']").length>0){
			jQuery(".left-links a[href='#"+$hash_value[1]+"']").trigger("click");
		}
	}
	if (jQuery('.bulletini-upload-add-button').length > 0) {
		if ( typeof wp !== 'undefined' && wp.media && wp.media.editor) {
			jQuery('.bulletini-upload-add-button').on( 'click', function( event ){
				var frame;
				event.preventDefault();
				if ( frame ) {
					frame.open();
					return;
				}
				metaBox = jQuery(this).parents('.bulletini-upload-container'), // Your meta box id here
			  	imgContainer = metaBox.find( '.bulletini-image-container'),
			  	imgIdInput = metaBox.find( '.bulletini-upload-field-id' );
				addImgLink = jQuery(this),
      			delImgLink = metaBox.find( '.bulletini-upload-delete-button'),
				frame = wp.media({
					title: 'Select or Upload Media Of Your Chosen Persuasion',
					button: {
						text: 'Use this media'
					},
					multiple: false  // Set to true to allow multiple files to be selected
				});
				frame.on( 'select', function() {
					var attachment = frame.state().get('selection').first().toJSON();
					imgContainer.append( '<a href="'+attachment.url+'" target="_blank"><img src="'+attachment.url+'" alt="" /></a>' );
					imgIdInput.val( attachment.id );
					addImgLink.addClass( 'hidden' );
					delImgLink.removeClass( 'hidden' );
				});
				frame.open();
			});
			jQuery('.bulletini-upload-delete-button').on( 'click', function( event ){
				event.preventDefault();
				metaBox = jQuery(this).parents('.bulletini-upload-container'), // Your meta box id here
			  	imgContainer = metaBox.find( '.bulletini-image-container'),
			  	imgIdInput = metaBox.find( '.bulletini-upload-field-id' );
				addImgLink = metaBox.find( '.bulletini-upload-add-button'),
      			delImgLink = jQuery(this);
				imgContainer.html( '' );
				addImgLink.removeClass( 'hidden' );
				delImgLink.addClass( 'hidden' );
				imgIdInput.val('');
			});
		}
	}
	if(jQuery('.bulletini-save-settings').length>0){
		jQuery('.wct-colorpicker').wpColorPicker();
		jQuery('.bulletini-save-settings').click(function(e){
			e.preventDefault();
			$bulletini_btn=jQuery(this);
			$bulletini_box=jQuery(this).parents('.bulletini_fancybox_wrapper');
			$bulletini_btn.hide();
			$bulletini_btn.before('<span class="bulletini_loading">Submitting...   </span>');
			$bulletini_data={
				'action': 'bulletini_save_template_settings'
			};
			$bulletini_box.find(":input").each(function(){
				$input=jQuery(this);
				$bulletini_data[$input.attr("id")]=$input.val();
			});
			jQuery.post($bulletini_ajax_object.ajax_url, $bulletini_data, function($bulletini_response) {
				$bulletini_message_container=$bulletini_box.find('.bulletini_message');
				if($bulletini_message_container.length>0){
					$bulletini_btn.before('<span class="bulletini_message"></span>');
				}
				if($bulletini_response==1){
					$bulletini_message_container.html('Settings Saved!   ');
				}
				else{
					$bulletini_message_container.html('Error is saving settings. Try again!   ');
				}
				$bulletini_box.find('.bulletini_loading').remove();
				$bulletini_btn.show();
			});
		});
	}
});
angular.module('bulletiniLog', [])
  	.controller('bulletiniLogController', function ($scope, $http) {
		var bulletiniLogInstance=this;
		bulletiniLogInstance.logrecords = [];
		bulletiniLogInstance.page=1;
		bulletiniLogInstance.loading=false;
		bulletiniLogInstance.addRecords = function(records) {
			bulletiniLogInstance.logrecords=records;
		}
		bulletiniLogInstance.getRecords = function() {
			bulletiniLogInstance.loading=true;
			$http({
				method: 'POST',
				url: $bulletini_ajax_object.ajax_url,
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				transformRequest: function(obj) {
					var str = [];
					for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
					return str.join("&");
				},
				data: {action: 'bulletini_get_sync_log', page: bulletiniLogInstance.page, rand: Math.random()}
			}).success(function ($response) {
				bulletiniLogInstance.addRecords($response);
				bulletiniLogInstance.loading=false;
			});
		}
		bulletiniLogInstance.nextRecords = function() {
			bulletiniLogInstance.page++;
			bulletiniLogInstance.getRecords();
		}
		bulletiniLogInstance.prevRecords = function() {
			bulletiniLogInstance.page--;
			bulletiniLogInstance.getRecords();
		}
		bulletiniLogInstance.getRecords();
	}
)