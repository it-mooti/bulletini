<?php
class bulletini_settings{
	private $post_fields=array(), $op_tags;
	public $notices=array();
	private $fields_map=array(
		"articles"=>array(
			"a1"=>array(
				"title"=>"",
				"image"=>"",
				"desc"=>"",
				"author"=>"",
				"link"=>"",
			),
			"b1"=>array(
				"title"=>"",
				"image"=>"",
				"desc"=>"",
				"author"=>"",
				"link"=>"",
			),
			"b2"=>array(
				"title"=>"",
				"image"=>"",
				"desc"=>"",
				"author"=>"",
				"link"=>"",
			),
			"b3"=>array(
				"title"=>"",
				"image"=>"",
				"desc"=>"",
				"author"=>"",
				"link"=>"",
			),
			"c1"=>array(
				"title"=>"",
				"image"=>"",
				"desc"=>"",
				"author"=>"",
				"link"=>"",
			),
			"c2"=>array(
				"title"=>"",
				"image"=>"",
				"desc"=>"",
				"author"=>"",
				"link"=>"",
			),
			"c3"=>array(
				"title"=>"",
				"image"=>"",
				"desc"=>"",
				"author"=>"",
				"link"=>"",
			)
		),
		"past_post"=>"",
		"bulletini_interests"=>"",
	);
   	public function __construct(){
		add_action('init', array($this, 'init'));
		add_action('wp_ajax_bulletini_get_sync_log', array($this, 'get_sync_log_ajax'));
	}
	public function init(){
		$args = array(
		  	'public' => false,
		  	'label'  => 'Books',
			'supports'=>array('title', 'editor')
		);
		register_post_type( 'bulletini_sync_log', $args );
		$this->save_settings();
		$this->op_tags=maybe_unserialize(get_option('bulletini_op_tags'));
		$this->op_contact_fields=maybe_unserialize(get_option('bulletini_op_contact_fields'));
		add_action('admin_enqueue_scripts', array($this, 'admin_scripts'));
		add_action('admin_menu', array($this, 'menu'));
		if(bulletini_option('add_featured_field')=="1"){
			add_action( 'add_meta_boxes', array($this, 'bulletini_meta_box'));
			add_action( 'save_post', array($this, 'bulletini_meta_box_data'));
		}
		if(bulletini_option('add_interests')=="1"){
			$bulletini_taxonomy=bulletini_option('taxonomy');
			if(!empty($bulletini_taxonomy)){
				add_action( $bulletini_taxonomy.'_add_form_fields', array($this, 'bulletini_extra_fields'), 10, 2 );
				add_action( $bulletini_taxonomy.'_edit_form_fields', array($this, 'bulletini_extra_fields'), 10, 2 );
				add_action( 'edited_'.$bulletini_taxonomy, array($this, 'bulletini_extra_fields_save'), 10, 2 );  
				add_action( 'create_'.$bulletini_taxonomy, array($this, 'bulletini_extra_fields_save'), 10, 2 );
			}
		}
	}
	public function add_sync_log($type, $log_text, $sync_log_action){
		wp_insert_post(array(
			'post_title'=>$type." - ".$sync_log_action,
			'post_content'=>$log_text,
			'post_type'=>'bulletini_sync_log',
			'post_status'=>'publish'
		));
	}
	public function get_sync_log($offset=1){
		$sync_logs=array();		
		$posts=get_posts(array('post_type'=>'bulletini_sync_log', 'posts_per_page'=>10, 'offset'=> $offset));
		foreach($posts as $post){
			$type=explode(" - ", $post->post_title);
			$sync_logs[]=array(
				'type'=>$type[0],
				'trigger'=>$type[1],
				'log_text'=>$post->post_content,
				'timestamp'=>$post->post_date
			);
		}
		return $sync_logs;
	}
	public function get_sync_log_ajax(){
		if(isset($_POST["page"])){
			$offset=(absint($_POST["page"])-1)*10+1;
		}
		else{
			$offset=1;
		}
		echo json_encode($this->get_sync_log($offset));
		wp_die();
	}
	public function get_sync_count(){
		return (int)bulletini_option('sync_count');
	}
	public function set_sync_count($count){
		update_option('bulletini_sync_count', $count);
	}
	public function listen_request(){
		
	}
	function bulletini_meta_box(){
		$post_type=bulletini_option('post_type');
		if(!empty($post_type)){
			add_meta_box(
				'bulletini_meta_box',
				'Bulletini',
				array($this, 'bulletini_meta_box_callback'),
				$post_type,
				'side',
				'high'
			);
		}
	}
	public function bulletini_meta_box_callback($post) {
		wp_nonce_field( 'bulletini_meta_box', 'bulletini_meta_box_nonce' );
		?>
		<div class="bulletini-fields-event">
            <?php $bulletini_is_featured = get_post_meta( $post->ID, 'bulletini_is_featured', true);?>
            <label for="bulletini_is_featured">Featured Article?</label>
            <div class="input_area">
                <input type="radio" name="bulletini_is_featured" id="bulletini_is_featured_no" class="bulletini_checkboxn" value=""<?php echo $bulletini_is_featured==""?' checked="checked"':""?> /> <label for="bulletini_is_featured_no">No</label>
                <input type="radio" name="bulletini_is_featured" id="bulletini_is_featured_yes" class="bulletini_is_featured" value="1"<?php echo $bulletini_is_featured=="1"?' checked="checked"':""?> /> <label for="bulletini_is_featured_yes">Yes</label>
                <small>Select yes to assign this as Featured Article. It is unique field. All other featured articles will be unassigned.</small>             	
            </div>
        </div>
		<?php
	}
	public function bulletini_meta_box_data( $post_id ) {
		$post_type=bulletini_option('post_type');
		if(!empty($post_type)){
			if ( isset( $_POST['post_type'] ) && $post_type == esc_attr($_POST['post_type']) ) {
				if ( ! isset( $_POST['bulletini_meta_box_nonce'] ) ) {
					return;
				}
				if ( ! wp_verify_nonce( $_POST['bulletini_meta_box_nonce'], 'bulletini_meta_box' ) ) {
					return;
				}
				if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
					return;
				}
				if ( ! current_user_can( 'edit_post', $post_id ) ) {
					return;
				}
				if(isset($_POST["bulletini_is_featured"]) && $_POST["bulletini_is_featured"]==1){
					$featured_posts=get_posts(array("post_type"=>$post_type, "meta_key"=>"bulletini_is_featured", "meta_value"=>"1"));
					foreach($featured_posts as $p){
						delete_post_meta($p->ID, 'bulletini_is_featured');
					}
					update_post_meta( $post_id, 'bulletini_is_featured', '1');
				}
			}
		}
	}
	public function bulletini_extra_fields($term="") {
		$bulletini_meta = array();
		$interests = "";
		if(is_object($term)){
			$interests = get_term_meta( $term->term_id, "bulletini_interests");
		}
		if(empty($interests)){
			$interests=array();
		}
		if(is_object($term)){
			?>
			<tr class="form-field">
				<th scope="row"><label for="bulletini_meta_interests">Interests</label></th>
                <th>
			<?php
		}
		else{
			?>
			<div class="form-field">
	            <label for="bulletini_meta_interests">Interests</label>
			<?php
		}
		$bulletini_interests=$this->get_custom_interests();
		?>
		<select data-placeholder="Choose interests..." name="bulletini_meta[interests][]" id="bulletini_meta_interests" multiple="multiple" class="bulletini_inputbox bulletini_selectbox">
			<?php
			foreach($bulletini_interests as $bulletini_interest){
				$bulletini_interest=trim($bulletini_interest);
				?>
				<option value="<?php echo $bulletini_interest?>"<?php echo in_array($bulletini_interest, $interests)?' selected="selected"':''?>><?php echo $bulletini_interest?></option>
				<?php
			}
			?>
		</select>
		<input type="hidden" name="bulletini_meta[id]" value="1" />
		<?php
        if(is_object($term)){
			?>
				</th>
            </tr>
			<?php
		}
		else{
			?>
			</div>
			<?php
		}
	}
	public function bulletini_extra_fields_save( $term_id ) {
		if ( isset( $_POST['bulletini_meta'] ) ) {
			if ( isset ( $_POST['bulletini_meta']["interests"] ) ) {
				$interests = $_POST['bulletini_meta']["interests"];
			}
			else{
				$interests = array();
			}
			delete_term_meta($term_id, "bulletini_interests");
			foreach($interests as $interest){
				add_term_meta($term_id, "bulletini_interests", sanitize_text_field($interest));
			}
		}
	}
	public function get_custom_interests(){
		$interests=explode("\n", bulletini_option('custom_interests'));
		$valid_interests=array();
		foreach($interests as $interest){
			$interest=trim(str_replace(array("\n", "\r"), array("", ""), $interest));
			if(!empty($interest)){
				$valid_interests[]=$interest;
			}
		}
		return $valid_interests;
	}
	public function set_op_tags($tags){
		$this->op_tags=$tags;
		update_option('bulletini_op_tags', maybe_serialize($this->op_tags));
	}
	public function get_op_tags(){
		return $this->op_tags;
	}
	public function set_op_contact_fields($contact_field){
		$this->op_contact_fields=$contact_field;
		update_option('bulletini_op_contact_fields', maybe_serialize($this->op_contact_fields));
	}
	public function get_op_contact_fields(){
		return $this->op_contact_fields;
	}
	public function get_fields_map(){
		$bulletini_no_of_fields=bulletini_option('no_of_fields');
		if($bulletini_no_of_fields!=7){
			unset($this->fields_map["articles"]["c3"]);
			if($bulletini_no_of_fields!=6){
				unset($this->fields_map["articles"]["c2"]);
				if($bulletini_no_of_fields!=5){
					unset($this->fields_map["articles"]["c1"]);
					unset($this->fields_map["articles"]["b3"]);
				}
			}
		}
		$this->fields_map=$this->field_map_value($this->fields_map);
		return $this->fields_map;
	}
	public function admin_scripts() {
        wp_enqueue_style( 'bulletini_admin_style', plugin_dir_url(__FILE__)."admin/style.css");
		wp_enqueue_style( 'bulletini_admin_datetime', plugin_dir_url(__FILE__)."admin/datetime.css" );
		wp_enqueue_style( 'bulletini_admin_fancybox', plugin_dir_url(__FILE__)."admin/fancybox/jquery.fancybox.css" );
		wp_enqueue_style('jquery-style', '//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css');
		wp_enqueue_style( 'bulletini_chosen_style', plugin_dir_url(__FILE__)."admin/chosen/chosen.min.css");
		wp_enqueue_style( 'wp-color-picker' );
		wp_enqueue_script( 'bulletini_admin_datetime', plugin_dir_url(__FILE__)."admin/datetime.js", array('jquery-ui-core', 'jquery-ui-slider', 'jquery-ui-datepicker', 'jquery-ui-selectmenu', 'wp-color-picker') );
		wp_enqueue_script( 'bulletini_chosen_style', plugin_dir_url(__FILE__)."admin/chosen/chosen.jquery.min.js");
		wp_enqueue_script( 'bulletini_admin_fancybox', plugin_dir_url(__FILE__)."admin/fancybox/jquery.fancybox.pack.js");
		wp_enqueue_script( 'bulletini_angular', plugin_dir_url(__FILE__)."admin/angular.min.js");
		wp_enqueue_script( 'bulletini_admin_script', plugin_dir_url(__FILE__)."admin/js.js");
		wp_localize_script( 'bulletini_admin_script', '$bulletini_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php')));
		wp_enqueue_media();
    }
	public function menu(){
        // This page will be under "Settings"
        add_menu_page(
			'Bulletini', 
            'Bulletini', 
            'manage_options', 
            'bulletini', 
            array( $this, 'create_admin_page' ),
			plugins_url('admin/images/logo-icon.png', __FILE__)
        );
    }
	public function save_settings(){
		if(count($_POST)>0 && isset($_POST["submit"])){
			if ( ! isset( $_POST['bulletini_settings_meta_box_nonce'] ) ) {
				return;
			}
			if ( ! wp_verify_nonce( $_POST['bulletini_settings_meta_box_nonce'], 'bulletini_settings_meta_box' ) ) {
				return;
			}
			if ( ! current_user_can( 'manage_options' ) ) {
				return;
			}
			//Check if API info has changed?
			$app_id=bulletini_option('app_id');
			$api_key=bulletini_option('api_key');
			if(isset($_POST["bulletini_app_id"]) &&  isset($_POST["bulletini_api_key"])){
				if(!empty($_POST["bulletini_app_id"]) && !empty($_POST["bulletini_app_id"])){
					if($app_id!=$_POST["bulletini_app_id"] || $app_id!=$_POST["bulletini_api_key"]){
						$ontraport=new bulletini_ontraport(sanitize_text_field($_POST["bulletini_app_id"]), sanitize_text_field($_POST["bulletini_api_key"]));
						if(!$ontraport->is_valid()){
							$this->add_notice('APP ID or API Key is not valid. Please confirm and submit again.', 'error', 'ConnectToONTRAPORT');
						}
					}
				}
				else{
					$this->add_notice('Enter APP ID and API Key to continue.', 'error', 'ConnectToONTRAPORT');
				}
			}
			foreach($_POST as $k=>$v){
				if(strpos($k, 'bulletini_')!==false){
					if(in_array($k, array('bulletini_custom_interests'))){
						$v=esc_textarea($v);
					}
					else{
						$v=$this->sanitize_text_field($v);
					}
					update_option($k, $v);
				}
			}
			$this->add_notice('Settings saved successfully.', 'message');
		}
	}
	public function sanitize_text_field($value){
		if(is_array($value)){
			$return=array();
			foreach($value as $k=>$v){
				$return[$k]=$this->sanitize_text_field($v);
			}
			return $return;
		}
		else{
			return sanitize_text_field($value);
		}
	}
	public function create_admin_page(){
		?>        
        <div class="wrap">
        	<form method="post" class="bulletini-fields not-expanded">
           	  	<?php wp_nonce_field( 'bulletini_settings_meta_box', 'bulletini_settings_meta_box_nonce' );?>
                <h3><img src="<?php echo plugins_url('admin/images/header_logo.png', __FILE__);?>" class="bulletini_header_logo"><span></span></h3>
                <div class="info_bar">
                	<?php submit_button();?>
              	</div>
                <?php $this->print_notices('general')?>
                <ul class="left-links">
                	<li class="menu-license"><a href="#content-license">License</a></li>
					<?php
					$app_id=bulletini_option('app_id');
					$api_key=bulletini_option('api_key');
					$ontraport=new bulletini_ontraport($app_id, $api_key);
                    if(bulletini_authenticated()){
						?>
						<li class="menu-api"><a href="#content-configuration">API Configuration</a></li>
                        <?php
                        if($ontraport->is_valid()){
							?>
                            <li class="menu-content-type"><a href="#content-content-settings">Content Type Settings</a></li>
                            <li class="menu-content-number-settings"><a href="#content-configuration-settings">Content Number Settings</a></li>
                            <li class="menu-field-match"><a href="#content-fields-settings">Content Field Matching Settings</a></li>
                            <li class="menu-templates"><a href="#content-templates">Templates</a></li>
                            <li class="menu-utm-settings"><a href="#content-utm-fields">UTM Settings</a></li>
                            <li class="menu-content-sync-engine"><a href="#content-schedule">Content Sync Engine</a></li>
                            <li class="menu-advanced-settings"><a href="#content-advanced">Advanced</a></li>
							<?php
						}
					}
					?>
                    <li class="menu-support"><a href="https://itmooti.helpdocs.com/bulletini" target="_blank" class="bulletini_external_link">Support</a></li>
                </ul>
                <div class="right-content">
                	<div id="content-license" class="content-elements">
                    	<h4>License</h4>
    	            	<div class="form-table">
                            <?php $bulletini_license_key=bulletini_option('license_key');?>
                            <label>License Key</label>
                            <small>Enter your license to begin. <a href="http://bulletini.rocks" target="_blank">Click Here to buy a license or upgrade your account</a>. <br /><br />
                                Do you want to test out Bulletini before you buy a license? No problem. Use the code FREETRIAL to get 100 free contact syncs added to test out the plugin before your buy. <br /><br />
                            </small>
                            <label>What is a Contact Sync?</label>
                            <small>Each time that you merge content from your blog into a single contact in ONTRAPORT using Bulletini's Sync Engine, this is counted as a content sync. If you have 500 contacts and you want to send new content to them 4 times in a calendar month you will require an account with 2000 or more Contact Syncs per month. Once you hit your monthly limit the Sync Engine will be disabled until the start of the next calendar month. </small>
                            <label for="bulletini_license_key">Your License Key</label>
                            <div class="input_area">
                            	<input type="text" name="bulletini_license_key" id="bulletini_license_key" value="<?php echo esc_attr($bulletini_license_key)?>" class="bulletini_inputbox" />
                                <?php
								$message=bulletini_option("message");
								$this->add_notice('You have '.$this->get_sync_count().' contact syncs remaining.', 'information', 'license');
								if($message!=""){
									$this->add_notice($message, 'information', 'license');
								}
								$this->print_notices('license');
								?>
                           	</div>
                            <?php submit_button('Click here to refresh');?>
                      	</div>
                   	</div>
					<?php
                    if(bulletini_authenticated()){
						?>
						<div id="content-configuration" class="content-elements">
							<h4>API Configuration</h4>
							<div class="form-table">
                            	<label>Connect to ONTRAPORT</label>
                                <small>Find your ONTRAPORT API information in the administration section of your app. <a href="https://support.ontraport.com/hc/en-us/articles/217882248-API-in-ONTRAPORT" target="_blank">Click here for help on how to find your API</a>.</small>
                                <?php $bulletini_app_id=bulletini_option('app_id');?>
								<label for="bulletini_app_id">APP ID</label>
								<div class="input_area">
									<input type="text" name="bulletini_app_id" id="bulletini_app_id" value="<?php echo esc_attr($bulletini_app_id)?>" class="bulletini_inputbox" />
								</div>
								<?php $bulletini_api_key=bulletini_option('api_key');?>
								<label for="bulletini_api_key">API Key</label>
								<div class="input_area">
									<input type="text" name="bulletini_api_key" id="bulletini_api_key" value="<?php echo esc_attr($bulletini_api_key)?>" class="bulletini_inputbox" />
								</div>
                                <?php $this->print_notices('ConnectToONTRAPORT')?>
								<?php
								if(!$ontraport->is_valid()){
									submit_button();
								}
								?>
							</div>
						</div>
						<?php
						if($ontraport->is_valid()){
							?>
                            <div id="content-content-settings" class="content-elements">
                                <h4>Content Type Settings</h4>
                                <div class="form-table">
                                    <label>Content Type Settings</label>
                                    <small>In this section you are able to decide which post or page type from your WP site you will be using as content in Bulletini. If you have custom fields in your content then you can decide which fields you want to be using for each piece of content.</small>
                                    <?php $bulletini_post_type=bulletini_option('post_type', false, 'post');?>
                                    <label for="bulletini_post_type">Post Type</label>
                                    <div class="input_area">
                                        <select name="bulletini_post_type" id="bulletini_post_type" class="bulletini_inputbox bulletini_selectbox">
                                            <?php
                                            $bulletini_post_types=get_post_types();
                                            foreach($bulletini_post_types as $post_type){
                                                ?>
                                                <option value="<?php echo $post_type?>"<?php echo $bulletini_post_type==$post_type?' selected="selected"':''?>><?php echo $post_type?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                        <small>Listed above are all the content types you have in your Wordpress Site. Most blogs use 'Posts' for their content. We have allowed for the possibility that you may have a different post type that you want to use in your content.</small>                     	
                                    </div>
                                    <label>Content Fields</label>
                                    <small>Each piece of content that is used in your Newsletter can have 5 fields associated. Title, Photo, Description, Author, Link. Bulletini is setup already by default to match the standard Wordpress content fields from blog posts however if you have other custom fields you are using in your posts, then this is where you can override the standard settings.<br /><br />
                                    Use the dropdown fields below to choose which Wordpress Data point you want to associate to each of the 5 field types that will be created in ONTRAPORT for each piece of content.</small>
                                    <?php
									$default_content_fields=array(
                                        "title"=>"post_title",
                                        "image"=>"post_thumbnail",
                                        "desc"=>"post_content",
                                        "author"=>"post_author",
                                        "link"=>"permalink",
                                    );
                                    foreach(array(
                                        "title"=>"Title",
                                        "image"=>"Photo",
                                        "desc"=>"Description",
                                        "author"=>"Author",
                                        "link"=>"Link",
                                    ) as $k=>$v){
                                        $bulletini_field=bulletini_option($k.'_field', false, $default_content_fields[$k]);?>
                                        <label for="bulletini_<?php echo $k?>_field"><?php echo $v?> Field</label>
                                        <div class="input_area">
                                            <?php $this->field_select_box($k.'_field', $bulletini_field)?>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <?php $bulletini_add_featured_field=bulletini_option('add_featured_field');?>
                                    <label for="bulletini_add_featured_field">Add Featured Article Field?</label>
                                    <div class="input_area">
                                        <input type="radio" name="bulletini_add_featured_field" id="bulletini_add_featured_field_no" class="bulletini_checkboxn" value=""<?php echo $bulletini_add_featured_field==""?' checked="checked"':""?> /> <label for="bulletini_add_featured_field_no">No</label>
                                        <input type="radio" name="bulletini_add_featured_field" id="bulletini_add_featured_field_yes" class="bulletini_add_featured_field" value="1"<?php echo $bulletini_add_featured_field=="1"?' checked="checked"':""?> /> <label for="bulletini_add_featured_field_yes">Yes</label>
                                        <small>Select yes to add a Featured Field widget in your selected post type add/edit screen. You can then assign any one of your posts as the featured article that will be served in the 'A' section of your Newsletter.<br /><br />
                                        This can be used if you want to have an article that will be sent in the main A section of your newsletter to all of your contacts regardless of their individual interest preferences.</small>             	
                                    </div>
                                    <label>Filtering Content for your Contacts</label>
                                    <small>So that we can deliver content that is relevant for each of your contacts we need to have a relationship between what they are interested in and how your articles are categorized. Bulletini allows you to do this in two ways.<br /><br />
                                    1. Using the new Interests List Field in ONTRAPORT by adding Category names as options that your contacts can select from. For example if you already have 'Monkey' 'Dog' and 'Dance' as categories for your posts, then we simply add these as options in the [Interests] field in ONTRAPORT and then you can assign to each contact their interests which correlate directly with your existing categories.<br /><br />
                                    2. Super Categories - This option allows you to create new Super Categories that can be assigned to any one of your existing categories. For example the new Super Category could be 'Animals' and you then can assign this to the 'Monkey' and 'Dog' Category that you already have assigned to your blog posts. This option is good for when you have hundreds of categories in your blog that you want to group together for your ONTRAPORT contacts to select from a smaller list.. </small>
                                    <?php $bulletini_taxonomy=bulletini_option('taxonomy', false, 'category');?>
                                    <label for="bulletini_taxonomy">Taxonomy</label>
                                    <div class="input_area">
                                    	<small>This setting allows you to choose which category type you are using to filter content in your wordpress blog. The default setting is the standard Wordpress 'category' field that exists in all Posts however in case you have a different taxonomy structure in your Wordpress site you can override this setting here:</small> 
                                        <select name="bulletini_taxonomy" id="bulletini_taxonomy" class="bulletini_inputbox bulletini_selectbox">
                                            <?php
                                            $bulletini_taxonomies=get_taxonomies();
                                            foreach($bulletini_taxonomies as $taxonomy){
                                                ?>
                                                <option value="<?php echo $taxonomy?>"<?php echo $bulletini_taxonomy==$taxonomy?' selected="selected"':''?>><?php echo $taxonomy?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>                    	
                                    </div>
                                    <?php $bulletini_add_interests=bulletini_option('add_interests');?>
                                    <label for="bulletini_add_interests">Would you like to use Super Categories to be added to your Category add/edit screen?</label>
                                    <div class="input_area">
                                        <input type="radio" name="bulletini_add_interests" id="bulletini_add_interests_no" class="bulletini_checkboxn bulletini_toggle" data-target=".bulletini_custom_interests" value=""<?php echo $bulletini_add_interests==""?' checked="checked"':""?> /> <label for="bulletini_add_interests_no">No</label>
                                        <input type="radio" name="bulletini_add_interests" id="bulletini_add_interests_yes" class="bulletini_checkboxn bulletini_toggle" data-target=".bulletini_custom_interests" value="1"<?php echo $bulletini_add_interests=="1"?' checked="checked"':""?> /> <label for="bulletini_add_interests_yes">Yes</label>             	
                                    </div>
                                    <?php $bulletini_custom_interests=bulletini_option('custom_interests');?>
                                    <label for="bulletini_custom_interests" class="bulletini_custom_interests"<?php echo $bulletini_add_interests=="1"?' style="display: block"':""?>>Super Categories</label>
                                    <div class="input_area bulletini_custom_interests"<?php echo $bulletini_add_interests=="1"?' style="display: block"':""?>>
                                        <textarea name="bulletini_custom_interests" id="bulletini_custom_interests" class="bulletini_textarea"><?php echo esc_textarea($bulletini_custom_interests)?></textarea>
                                        <small>Add Super Categories here. Each in a new line.<br /><br />
                                         On save it automatically adds these options to the ONTRAPORT 'Bulletini Interests' field. <br /><br />
                                        <i>Important: These new options will be added to the 'Bulletini Interests' field in your ONTRAPORT account. If you edit or delete that field in ONTRAPORT it will break the connection with Wordpress that keeps this in sync.</i></small>             	
                                    </div>
                                </div>
                            </div>
                            <div id="content-configuration-settings" class="content-elements">
                                <h4>Content Number Settings</h4>
                                <div class="form-table">
                                <label>Content Number Settings</label>
                                <small>Bulletini takes specific parts of your content and merges it into new fields that we will dynamically create in your ONTRAPORT acocunt. Each piece of content requires 5 field. Title, Photo, Description, Author and Link. We have templates that can have 3, 5, 6 or 7 pieces of content merged.</small>
                                <?php $bulletini_no_of_fields=bulletini_option('no_of_fields');?>
                                <label for="bulletini_no_of_fields">How many pieces of content do you want in each newsletter?</label>
                                <div class="input_area">
                                    <select name="bulletini_no_of_fields" id="bulletini_no_of_fields" value="<?php echo $bulletini_no_of_fields?>" class="bulletini_selectbox">
                                        <option value="3"<?php if($bulletini_no_of_fields==3) echo ' selected="selected"';?>>3 (17 fields)</option>
                                        <option value="5"<?php if($bulletini_no_of_fields==5) echo ' selected="selected"';?>>5 (27 fields)</option>
                                        <option value="6"<?php if($bulletini_no_of_fields==6) echo ' selected="selected"';?>>6 (32 fields)</option>
                                        <option value="7"<?php if($bulletini_no_of_fields==7) echo ' selected="selected"';?>>7 (37 fields)</option>
                                    </select>
                                </div>
                                <?php $bulletini_no_of_fields_sync=bulletini_option('no_of_fields_sync');?>
                                <label for="bulletini_no_of_fields_sync">How many articles do you want to synchronize with ontraport?</label>
                                <div class="input_area">
                                    <select name="bulletini_no_of_fields_sync" id="bulletini_no_of_fields_sync" value="<?php echo $bulletini_no_of_fields_sync?>" class="bulletini_selectbox">
                                        <?php
                                        for( $i = 1; $i <= $bulletini_no_of_fields; $i++ ) {
											?>
                                            <option value="<?php echo $i?>"<?php if($bulletini_no_of_fields_sync==$i) echo ' selected="selected"';?>><?php echo $i?> Article(s)</option>
                                            <?php
										}
										?>
                                    </select>
                                </div>
                                <?php
								$this->print_notices('fieldsadd');
								?>
								<?php
                                $bulletini_past_post_value=bulletini_option("past_post");
								if(!empty($bulletini_app_id) && !empty($bulletini_api_key) && empty($bulletini_past_post_value)){
									?>
									<label>Ontraport Fields</label>
									<div class="input_area">
										<small>The next step is to create fields in your ONTRAPORT account for your Wordpress Content. The fields that will be created correlate with the content meta data that will be able to be merged into the Newsletter.<br /><br />
                                        Your ONTRAPORT account has a limited fields allowance. Bulletin mostly uses 'Long Text' fields so as to use as little of your allowance as possible. <a href="https://support.ontraport.com/hc/en-us/articles/217881218-Add-Contact-Fields-Sections#maximum" target="_blank">Read more about ONTRAPORT Custom Fields here:</a><br /><br />
                                        Here are a list of the Sections and Fields we will create along with their data types:<br /><br />
                                        <strong>Sections</strong> <br /><br />
										A maximum of 4 field sections will be created called:<br />
                                        BULLETINI SETTINGS (2 Fields)<br />
                                        BULLETINI - A: FEATURED ARTICLE (5 Fields)<br />
                                        BULLETINI - B: DYNAMIC CONTENT (15 Fields)<br />
                                        BULLETINI - C: DYNAMIC CONTENT (15 Fields)<br /><br />
                                        <strong>Content Fields:</strong><br /><br />
                                        Author  - Long Text<br />
                                        Title - Long Text<br />
                                        Image - Long Text<br />
                                        Description - Long Text<br />
                                        Link - Long Text<br /><br />
                                        <strong>Settings Fields:</strong><br /><br />
                                        Interests - List Selection<br />
                                        To list all the interests that your contacts can select relating to your content taxonomy<br /><br />
                                        Past Posts - Long Text<br />
										The Post numbers that have already been sent to a user will display here to ensure that the same content is not sent a second time.<br /><br />
                                        Once you have decided how many articles you want to merge on each contact syncs click the button below to create the fields.<br /><br />
										Click the button below to create the fields. <br /><br /><input type="submit" value="Create and Setup Fields Now!" class="button button-primary" id="submit" name="bulletini_fieldssetup"><br /><br />
											
										</small>
									</div>
									<?php
								}
								?>
                                </div>
                          	</div>                            
                            <div id="content-fields-settings" class="content-elements">
                                <h4>Content Field Matching Settings</h4>
                                <div class="form-table">
                                    <label>Content Field Matching Settings</label>
                                    <small>This section lists all the blog content data that will be passed to your contact fields. Bulletini automatically will match the content with fields created by Bulletini however you can override this by changing these settings to match content to any other field in your ONTRAPORT account from the dropdown..</small>
                                    <?php
                                    $this->field_map_field($this->get_fields_map());
                                    ?>
                                </div>
                            </div>
                            <div id="content-templates" class="content-elements">
                                <h4>Templates</h4>
                                <div class="form-table">
                                    <label>ONTRAPORT HTML Message Templates</label>
                                    <small>Bulletini's sync engine will take blog content from your Wordoress posts and add it into your ONTRAPORT contacts based on what each contact has listed under their interests. The Bulletini fields can then be used in any one of your messages to merge the relevant content in. <br /><br />
                                    Bulletini has configurable templates below that you can push into your account that are already setup with the correct Bullletini content merge fields.<br /><br />
                                    If you have your own design in mind then you can use these templates as a guide and then edit them within your ONTRAPORT account to make them as customized as needed.</small>
                                    <?php
                                    $template_path=plugin_dir_path( __FILE__ ).'../templates';
									$templates = scandir($template_path);
									sort($templates);
									foreach ($templates as $template) {
                                        if ($template != "." && $template != ".." && is_dir($template_path."/".$template)) {
											$bulletini_template_message_id=bulletini_option('template_'.$template.'_message_id');
											$config_data=array();
											$config_file=plugin_dir_path( __FILE__ ).'../templates/'.$template."/config.xml";
											if(file_exists($config_file)){
												$config_data=simplexml_load_string(file_get_contents($config_file));
												if(!$config_data){
													$config_data=array();
												}
												else{
													$config_data=(array)$config_data;
												}
											}
											?>
											<div class="bulletini-template-container clearfix">
												<label style="display:none" for="bulletini_post_type"><?php echo isset($config_data["name"])?$config_data["name"]:$template;?></label>
												<div class="input_area">
													<?php $this->print_notices('Template'.$template);?>
													<div class="bulletini-template-thumbnail"><a href="<?php echo plugin_dir_url( __FILE__ ).'../templates/'.$template."/screenshot.png"?>" target="_blank"><img src="<?php echo plugin_dir_url( __FILE__ ).'../templates/'.$template."/screenshot.png"?>" /></a></div>
													<div class="bulletini-template-details">
														<ul>
															<li><span class="bulletini-template-label">Template Name: </span> <?php echo isset($config_data["name"])?$config_data["name"]:$template;?></li>
															<li><p><?php echo isset($config_data["description"])?$config_data["description"]:'';?></p></li>
															<li><a class="button button-primary bulletini_template_options_page" href="#bulletini_fancybox_wrapper_<?php echo $template?>" data-template="<?php echo $template?>">Settings</a>
																<?php
																if(!empty($bulletini_template_message_id)){
																	?>
																	<a class="button button-primary" href="https://app.ontraport.com/#!/message/edit&id=<?php echo $bulletini_template_message_id?>" target="_blank">Edit Message</a>
																	<a class="button button-primary" href="<?php echo wp_nonce_url( admin_url('admin.php?page=bulletini&bulletini_uninstall_template&template='.$template), 'uninstall-template_'.$template, 'bulletini_uninstall_template_nonce' );?>#content-templates" onclick="return confirm('Are you sure that you want to permanently delete this template from your ONTRAPORT account?')">Uninstall</a>
																	<?php
																}
																else{
																	?>
																	<a class="button button-primary" href="<?php echo wp_nonce_url( admin_url('admin.php?page=bulletini&bulletini_install_template&template='.$template), 'install-template_'.$template, 'bulletini_install_template_nonce' );?>#content-templates">Install</a>
																	<?php
																}?></li>
														</ul>
													</div> 
												</div>
												<div style="display:none">
													<div class="bulletini_fancybox_wrapper" id="bulletini_fancybox_wrapper_<?php echo $template?>">
														<div class="bulletini-fields not-expanded">
															<h3>Settings<span></span></h3>
															<input type="hidden" name="bulletini_template" id="bulletini_template" value="<?php echo $template?>" />
															<div class="form-table">
																<?php
																if(isset($config_data["configuration"])){
																	foreach($config_data["configuration"] as $field_type=>$field_key){
																		$field_input_key='bulletini_'.$template."_".str_replace("-", "_", sanitize_title($field_key));
																		?>
																		<label for="<?php echo $field_input_key?>"><?php echo $field_key?></label>
																		<div class="input_area">
																			<?php
																			switch($field_type){
																				case "text":
																					?>
																					<input type="text" name="<?php echo $field_input_key?>" id="<?php echo $field_input_key?>" value="<?php echo esc_attr(get_option($field_input_key))?>" />
																					<?php
																				break;
																				case "color":
																					?>
																					<input type="text" name="<?php echo $field_input_key?>" id="<?php echo $field_input_key?>" value="<?php echo esc_attr(get_option($field_input_key))?>" class="wct-colorpicker" />
																					<?php
																				break;
																				case "textarea":
																					?>
																					<textarea name="<?php echo $field_input_key?>" id="<?php echo $field_input_key?>" ><?php echo esc_textarea(get_option($field_input_key))?></textarea>
																					<?php
																				break;
																				case "image":
																					$this->media_upload($field_input_key, esc_attr(get_option($field_input_key)));
																				break;
																			}
																			?>
																		</div>	
																		<?php
																	}
																	?>
																	<div class="clr"></div>
																	<div class="info_bar info_bar_bottom"><a class="button button-primary bulletini-save-settings" href="#">Save Settings</a></div>
																	<?php
																}
																else{
																	?>
																	<div class="message">No Settings defined for this template</div>
																	<?php
																}
																?>
															</div>
														</div>
													</div>
													<div class="clr"></div>
												</div>
											</div>
											<?php
                                        }
                                    }                            
                                    ?>
                                </div>
                            </div>
                            <div id="content-utm-fields" class="content-elements">
                                <h4>UTM Settings</h4>
                                <div class="form-table">
                                    <?php $bulletini_enable_utms=bulletini_option('enable_utms');?>
                                    <label>What Are UTM Parameters</label>
                                    <small>UTM parameters are simply tags that you add to a URL. When someone clicks on a URL with UTM parameters, those tags are sent back to your Google Analytics for tracking. Then within GA you can see which Emails, Content and Links were more successful at bringing contacts back to your website.<br /><br />
                                    The Bulletini templates add links to your content from the images, title and buttons. Switch this setting to 'Yes' if you would like to add tags to have each of the links in your Newsletter.<br /><br />
                                    The tags are added as shown below.<br /><br />
                                    <strong>utm_source=bulletini</strong><br />
                                    <strong>utm_medium=email</strong><br />
                                    <strong>utm_campaign=YYMMDD</strong> eg. 160915 for 15th of September 2016<br />
                                    <strong>utm_content=1234</strong> this is the WP post number<br />
                                    <strong>utm_term=A1</strong> (this is in reference to the placement of the link within the newsletter)<br /><br />
                                    These are not editable and so can only be turned on or off.
                                    </small>
                                    <label for="bulletini_enable_utms">Enable UTM Tracking?</label>
                                    <div class="input_area">
                                        <input type="radio" name="bulletini_enable_utms" id="bulletini_enable_utms_no" class="bulletini_checkboxn" value=""<?php echo $bulletini_enable_utms==""?' checked="checked"':""?> /> <label for="bulletini_enable_utms_no">No</label>
                                        <input type="radio" name="bulletini_enable_utms" id="bulletini_enable_utms_yes" class="bulletini_add_featured_field" value="1"<?php echo $bulletini_enable_utms=="1"?' checked="checked"':""?> /> <label for="bulletini_enable_utms_yes">Yes</label>
                                    </div>
                                </div>
                            </div>
                            <div id="content-schedule" class="content-elements">
                                <h4>Content Sync Engine</h4>
                                <div class="form-table" ng-app="bulletiniLog">
                                	<label>Content Sync Engine</label>
                                    <small>In this section you can manually run the sync engine that takes the content from your posts, adding it into your ONTRAPORT contacts based on the configurations you have made.</small>
                                    <label>Content Filter</label>
                                    <small>Each time that Bulletini runs the Sync Engine it can add the content only to contacts that have a particular tag. You can leave this empty to have no tag filter.</small>
                                    <?php $bulletini_contact_tag=bulletini_option('contact_tag');?>
                                    <label for="bulletini_contact_tag">What tag would you like to use as a filter?</label>
                                    <div class="input_area">
                                        <?php
                                        $tags=$this->get_op_tags();
                                        if(is_array($tags) && count($tags)>0){
                                            ?>
                                            <select name="bulletini_contact_tag" id="bulletini_contact_tag" class="bulletini_inputbox bulletini_selectbox">
                                                <option value="">Select Tag</option>
                                                <?php
                                                foreach($tags as $tag){
                                                    ?>
                                                    <option value="<?php echo $tag->tag_id?>"<?php if($tag->tag_id==$bulletini_contact_tag) echo ' selected="selected"';?>><?php echo $tag->tag_name?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                            <?php
                                        }
                                        else{
                                            ?>
                                            <input type="text" name="bulletini_contact_tag" id="bulletini_contact_tag" value="<?php echo $bulletini_contact_tag?>" class="bulletini_inputbox" />
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <label>On Demand</label>
                                    <small><input type="submit" value="Click Here to Initiate Bulletini Script Now" class="button button-primary" id="bulletini_ondemandscript" name="bulletini_ondemandscript"></small>
                                    <label>Scheduled Syncs</label>
                                    <div class="input_area">
                                        <small>If you would like the Sync Engine to run automatically you can use this setting to run the Sync Engine at specific intervals.</small>
                                        <?php $this->print_notices('OnDemandScript');?>
                                        <?php $bulletini_cron_schedule=bulletini_option('cron_schedule');?><br />
                                        Cron Runs <div style="width: 180px; display:inline-block"><select name="bulletini_cron_schedule" class="bulletini_selectbox bulletini_cron_schedule">
                                            <option value=""<?php echo ($bulletini_cron_schedule=="")?' selected="selected"':""?>>Select one</option>
                                            <option value="daily"<?php echo ($bulletini_cron_schedule=="daily")?' selected="selected"':""?>>Daily</option>
                                            <option value="weekly"<?php echo ($bulletini_cron_schedule=="weekly")?' selected="selected"':""?>>Weekly</option>
                                            <option value="monthly"<?php echo ($bulletini_cron_schedule=="monthly")?' selected="selected"':""?>>Monthly</option>
                                        </select></div>
                                        <div class="bulletini_cron_schedule_settings bulletini_cron_schedule_daily">
                                            <?php $bulletini_cron_schedule_daily_days=bulletini_option('cron_schedule_daily_days');?>
                                            Runs Every
                                            <div style="width: 100px; display:inline-block"><select name="bulletini_cron_schedule_daily_days" id="bulletini_cron_schedule_daily_days" class="bulletini_selectbox">
                                                <?php
                                                for($i=1; $i<=10; $i++){
                                                    ?>
                                                    <option value="<?php echo $i?>"<?php echo $bulletini_cron_schedule_daily_days==$i?' selected="selected"':'';?>><?php echo $i?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select></div> days
                                            <div style="padding-top:10px;">
												<?php $bulletini_cron_schedule_daily_start_date=bulletini_option('cron_schedule_daily_start_date');?>
                                                Starts on <input type="text" name="bulletini_cron_schedule_daily_start_date" id="bulletini_cron_schedule_daily_start_date" value="<?php echo $bulletini_cron_schedule_daily_start_date?>" class="bulletini_datefield" />
                                                at <div style="display: inline-block; width: 100px;">
                                                    <?php $bulletini_cron_schedule_daily_time=bulletini_option('cron_schedule_daily_time');?>
                                                    <select name="bulletini_cron_schedule_daily_time" id="bulletini_cron_schedule_daily_time" class="bulletini_selectbox">
                                                        <?php
                                                        for($i=0; $i<24; $i++){
                                                            $time_string=bulletini_time_string($i);
                                                            ?>
                                                            <option<?php echo $bulletini_cron_schedule_daily_time==$i? ' selected="selected"':'';?> value="<?php echo $i?>"><?php echo $time_string;?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                             	</div>
                                            </div>
                                            <div style="padding-top:10px;">
                                            	<?php
												$next_run=bulletini_next_run("daily");
												if($next_run){
													$this->add_notice("Next Scheduled Sync will be on ".date("F j, Y h:i A", $next_run),'information','schedule_daily');
													$this->print_notices('schedule_daily');
												}
												?>
                                            </div>
                                        </div>
                                        <div class="bulletini_cron_schedule_settings bulletini_cron_schedule_weekly">
                                            <?php $bulletini_cron_schedule_weekly_weeks=bulletini_option('cron_schedule_weekly_weeks');?>
                                            Runs Every
                                            <div style="width: 100px; display:inline-block"><select name="bulletini_cron_schedule_weekly_weeks" id="bulletini_cron_schedule_weekly_weeks" class="bulletini_selectbox">
                                                <?php
                                                for($i=1; $i<=20; $i++){
                                                    ?>
                                                    <option value="<?php echo $i?>"<?php echo $bulletini_cron_schedule_weekly_weeks==$i?' selected="selected"':'';?>><?php echo $i?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select></div> weeks<br />
                                            <?php $bulletini_cron_schedule_weekly_days=bulletini_option('cron_schedule_weekly_days');
                                            if(!is_array($bulletini_cron_schedule_weekly_days)){
                                                $bulletini_cron_schedule_weekly_days=array();
                                            }
                                            ?>
                                            <div style="padding-top:10px;">
                                                Runs on
                                                <div style="display:inline-block; min-width:320px;"><select name="bulletini_cron_schedule_weekly_days[]" id="bulletini_cron_schedule_weekly_days" class="bulletini_selectbox" multiple="multiple">
                                                    <?php
                                                    foreach(array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday") as $day){
                                                        ?>
                                                        <option value="<?php echo $day?>"<?php echo in_array($day, $bulletini_cron_schedule_weekly_days)?' selected="selected"':'';?>><?php echo $day?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select></div>
                                           	</div>
                                            <?php $bulletini_cron_schedule_weekly_start_date=bulletini_option('cron_schedule_weekly_start_date');?>
                                            <div style="padding-top:10px;">
                                            	Starts on <input type="text" name="bulletini_cron_schedule_weekly_start_date" id="bulletini_cron_schedule_weekly_start_date" value="<?php echo $bulletini_cron_schedule_weekly_start_date?>" class="bulletini_datefield" />
                                                at <div style="display: inline-block; width: 100px;">
                                                    <?php $bulletini_cron_schedule_weekly_time=bulletini_option('cron_schedule_weekly_time');?>
                                                    <select name="bulletini_cron_schedule_weekly_time" id="bulletini_cron_schedule_weekly_time" class="bulletini_selectbox">
                                                        <?php
                                                        for($i=0; $i<24; $i++){
                                                            $time_string=bulletini_time_string($i);
                                                            ?>
                                                            <option<?php echo $bulletini_cron_schedule_weekly_time==$i? ' selected="selected"':'';?> value="<?php echo $i?>"><?php echo $time_string;?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                             	</div>
                                          	</div>
                                            <div style="padding-top:10px;">
                                            	<?php
												$next_run=bulletini_next_run("weekly");
												if($next_run){
													$this->add_notice("Next Scheduled Sync will be on ".date("F j, Y h:i A", $next_run),'information','schedule_weekly');
													$this->print_notices('schedule_weekly');
												}
												?>
                                            </div>
                                        </div>
                                        <div class="bulletini_cron_schedule_settings bulletini_cron_schedule_monthly">
                                            <?php $bulletini_cron_schedule_monthly_months=bulletini_option('cron_schedule_monthly_months');?>
                                            Runs Every
                                            <div style="display: inline-block; width: 100px;"><select name="bulletini_cron_schedule_monthly_months" id="bulletini_cron_schedule_monthly_months" class="bulletini_selectbox">
                                                <?php
                                                for($i=1; $i<=12; $i++){
                                                    ?>
                                                    <option value="<?php echo $i?>"<?php echo $bulletini_cron_schedule_monthly_months==$i?' selected="selected"':'';?>><?php echo $i?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select></div> months<br />
                                            <?php $bulletini_cron_schedule_monthly_start_date=bulletini_option('cron_schedule_monthly_start_date');?>
                                            <div style="padding-top:10px;">
                                                Starts on <input type="text" name="bulletini_cron_schedule_monthly_start_date" id="bulletini_cron_schedule_monthly_start_date" value="<?php echo $bulletini_cron_schedule_monthly_start_date?>" class="bulletini_datefield" />
                                                at <div style="display: inline-block; width: 100px;">
                                                    <?php $bulletini_cron_schedule_monthly_time=bulletini_option('cron_schedule_monthly_time');?>
                                                    <select name="bulletini_cron_schedule_monthly_time" id="bulletini_cron_schedule_monthly_time" class="bulletini_selectbox">
                                                        <?php
                                                        for($i=0; $i<24; $i++){
                                                            $time_string=bulletini_time_string($i);
                                                            ?>
                                                            <option<?php echo $bulletini_cron_schedule_monthly_time==$i? ' selected="selected"':'';?> value="<?php echo $i?>"><?php echo $time_string;?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                           	</div>
                                            <div style="padding-top:10px;">
                                            	<?php
												$next_run=bulletini_next_run("monthly");
												if($next_run){
													$this->add_notice("Next Scheduled Sync will be on ".date("F j, Y h:i A", $next_run),'information','schedule_monthly');
													$this->print_notices('schedule_monthly');
												}
												?>
                                            </div>
                                        </div>
                                    </div>     
                                    <label>History Log</label>
                                    <div class="input_area" style="position:relative" ng-controller="bulletiniLogController as bulletiniLogInstance">
	                                    <div class="bulletini_loading" ng-show="bulletiniLogInstance.loading"></div>
                                        <small><span class="buttetini_history_log_indicator buttetini_history_log_indicator_scheduled">Scheduled</span> <span class="buttetini_history_log_indicator buttetini_history_log_indicator_ondemand">On Demand</span></small>
                                        <table width="100%" class="buttetini_history_log" cellspacing="0" cellpadding="0">
                                        	<tr>
                                            	<th style="width:20%">Date</th>
                                                <th>Log</th>
                                            </tr>
                                            <tr ng-repeat="logrecord in bulletiniLogInstance.logrecords" class="buttetini_history_log_{{logrecord.type}} buttetini_history_log_{{logrecord.trigger}}">
                                               	<td>{{logrecord.timestamp}}</td>
                                                <td>{{logrecord.log_text}}</td>
                                            </tr>
											<tr ng-show="bulletiniLogInstance.logrecords.length==0">
                                              	<td colspan="2" class="buttetini_notice buttetini_info">No History Logs</td>
                                            </tr>
                                            <tr>
                                              	<td colspan="2" class="buttetini_control_buttons"><span class="bulletini_prev" ng-show="bulletiniLogInstance.page>1" ng-click="bulletiniLogInstance.prevRecords()">Prev</span><span class="bulletini_next" ng-show="bulletiniLogInstance.logrecords.length==10" ng-click="bulletiniLogInstance.nextRecords()">Next</span></td>
                                            </tr>
                                        </table>
                                  	</div>
                                </div>
                            </div>
                            <div id="content-advanced" class="content-elements">
                                <h4>Advance Settings</h4>
                                <div class="form-table">
                                    <?php
                                    $bulletini_past_post_value=bulletini_option("past_post");
									if(!empty($bulletini_past_post_value)){
										?>
                                        <label>Ontraport Fields:</label>
										<small><input type="submit" value="Remove all Bulletini fields from your ONTRAPORT account" class="button button-secondary" id="bulletini_fieldsremove" name="bulletini_fieldsremove" onclick="return confirm('Are you sure that you want to remove all the Bulletini Fields from your ONTRAPORT account?')"></small>
										<?php
									}
									$this->print_notices('fieldsremove');
									?>
                                    <label>Create ONTRAPORT Fields</label>
                                    <?php $bulletini_no_of_fields2=bulletini_option('no_of_fields');?>
                                    <label for="bulletini_no_of_fields2">How many pieces of content do you want in each newsletter?</label>
                                    <div class="input_area">
                                        <select name="bulletini_no_of_fields2" id="bulletini_no_of_fields2" value="<?php echo $bulletini_no_of_fields2?>" class="bulletini_selectbox">
                                            <option value="3"<?php if($bulletini_no_of_fields==3) echo ' selected="selected"';?>>3 (17 fields)</option>
                                            <option value="5"<?php if($bulletini_no_of_fields==5) echo ' selected="selected"';?>>5 (27 fields)</option>
                                            <option value="6"<?php if($bulletini_no_of_fields==6) echo ' selected="selected"';?>>6 (32 fields)</option>
                                            <option value="7"<?php if($bulletini_no_of_fields==7) echo ' selected="selected"';?>>7 (37 fields)</option>
                                        </select>
                                    </div>
                                    <div class="input_area">
                                        <small>Click the button to create and setup the fields required for the plugin. (Note: All the previous Field Matching Settings will be overridden). <br /><br /><input type="submit" value="Create and Setup Fields Now!" class="button button-primary" id="bulletini_fieldssetup" name="bulletini_fieldssetup"></small>
                                    </div>
                                    <?php
                                    $this->print_notices('fieldsadd');
									?>
                                    <label>Cron Setup</label>
                                    <div class="input_area">
                                        <small>For better performance, Disbale WP Cron and setup manual cron job. Read this article <a href="https://tommcfarlin.com/wordpress-cron-jobs/" target="_blank">Properly Setting Up WordPress Cron Jobs</a></small>
                                    </div>
                                    <label>Clear ONTRAPORT API Cache</label>
                                    <div class="input_area">
                                        <small>Click the button to clear the cache of Tags and Contact Fields. <br /><br /><input type="submit" value="Clear Cache Now!" class="button button-primary" id="bulletini_rehash" name="bulletini_rehash"></small>
                                    </div>
                                </div>
                            </div>
                            <?php
						}
					}
					?>
           		</div>
                <div class="clr"></div>
                <?php $this->print_notices('general')?>
                <div class="info_bar info_bar_bottom">
                	<?php submit_button();?>
              	</div>
            </form>
        </div>
        <?php
    }
	public function media_upload($field_id, $field_value, $type='image', $post_id=null){
		$have_img=!empty($field_value);
		$upload_link = esc_url( get_upload_iframe_src($type,$post_id));
		?>
        <div class="bulletini-upload-container">
            <div class="bulletini-image-container">
                <?php
                if($have_img){
                    $img_src = wp_get_attachment_image_src( $field_value, 'full' );
                    ?>
                    <a href="<?php echo $img_src[0] ?>" target="_blank"><img src="<?php echo $img_src[0] ?>" alt="" /></a>
                    <?php
                }
                ?>
            </div>
            <p class="hide-if-no-js" data-fieldid="<?php echo $field_id?>">
                <a class="bulletini-upload-add-button button <?php if ($have_img) { echo 'hidden'; } ?>" 
                   href="<?php echo $upload_link ?>">
                    <?php _e('Set Image') ?>
                </a>
                <a class="bulletini-upload-delete-button button <?php if (!$have_img) { echo 'hidden'; } ?>" 
                  href="#">
                    <?php _e('Remove this image') ?>
                </a>
            </p>                                
            <input id="<?php echo $field_id?>" name="<?php echo $field_id?>" type="hidden" value="<?php echo esc_attr( $field_value ); ?>" class="bulletini-upload-field-id" />
      	</div>
        <?php
	}
	public function field_map_field($fields, $parent_key=""){
		foreach($fields as $k=>$v){
			$k=(!empty($parent_key)?$parent_key."_":"").$k;
			if(is_array($v)){
				$this->field_map_field($v, $k);
			}
			else{
				$contact_fields=$this->get_op_contact_fields();
				$field_value=bulletini_option($k);
				if(!empty($field_value)){
					$v=$field_value;
				}
				?>
                <label for="bulletini_<?php echo $k?>_field"><?php echo ucwords(str_replace("_", " ", $k))?></label>
				<div class="input_area">
                	<?php
                    if(count($contact_fields)>0){
						?>
						<select name="bulletini_<?php echo $k?>" id="bulletini_<?php echo $k?>" class="bulletini_inputbox bulletini_selectbox">
							<option value="">Select Field</option>
							<?php
							foreach($contact_fields as $k1=>$v1){
								?>
								<option value="<?php echo sanitize_key($k1)?>"<?php if($v==$k1) echo ' selected="selected"';?>><?php echo esc_attr($v1->alias)?></option>
								<?php
							}
							?>
						</select>
                        <?php
					}
					else{
						?>
						<input type="text" name="bulletini_<?php echo sanitize_key($k)?>" id="bulletini_<?php echo $k?>" class="bulletini_inputbox" value="<?php echo esc_attr($v)?>" />
						<?php
					}
					?>
				</div>
                <?php
			}
		}
	}
	public function field_map_value($fields, $parent_key=""){
		$fields_value=array();
		foreach($fields as $k=>$v){
			$k1=(!empty($parent_key)?$parent_key."_":"").$k;
			if(is_array($v)){
				$fields_value[$k]=$this->field_map_value($v, $k1);
			}
			else{
				$value=bulletini_option($k1);
				if(!empty($value)){
					$fields_value[$k]=$value;
				}
				else{
					$fields_value[$k]=$v;
				}
			}
		}
		return $fields_value;
	}
	public function field_select_box($key, $value){
		global $wpdb;
		if(count($this->post_fields)==0){
			$post_type=bulletini_option('post_type', false, 'post');
			$query = "
				SELECT DISTINCT($wpdb->postmeta.meta_key) 
				FROM $wpdb->posts 
				LEFT JOIN $wpdb->postmeta 
				ON $wpdb->posts.ID = $wpdb->postmeta.post_id 
				WHERE $wpdb->posts.post_type = '%s' 
				AND $wpdb->postmeta.meta_key != '' 
				AND $wpdb->postmeta.meta_key NOT RegExp '(^[_0-9].+$)' 
				AND $wpdb->postmeta.meta_key NOT RegExp '(^[0-9]+$)'
			";
			$this->post_fields = $wpdb->get_col($wpdb->prepare($query, $post_type));
		}
		?>
		<select name="bulletini_<?php echo $key?>" id="bulletini_<?php echo $key?>" class="bulletini_inputbox bulletini_selectbox">
			<option value="post_title"<?php echo $value=="post_title"?' selected="selected"':''?>>Post Title</option>
            <option value="post_content"<?php echo $value=="post_content"?' selected="selected"':''?>>Post Content</option>
            <option value="post_thumbnail"<?php echo $value=="post_thumbnail"?' selected="selected"':''?>>Post Thumbnail</option>
            <option value="post_author"<?php echo $value=="post_author"?' selected="selected"':''?>>Post Author</option>
            <option value="permalink"<?php echo $value=="permalink"?' selected="selected"':''?>>Permalink</option>
			<?php
            foreach($this->post_fields as $field_key){
				?>
				<option value="<?php echo $field_key?>"<?php echo $value==$field_key?' selected="selected"':''?>><?php echo $field_key?></option>
				<?php
			}
			?>
        </select>
		<?php
	}
	public function add_notice($text, $type='message', $location='general'){
		if(!isset($this->notices[$location][$type])){
			$this->notices[$location][$type]=array();
		}
		if(!in_array($text, $this->notices[$location][$type])){
			$this->notices[$location][$type][]=$text;
		}
	}
	public function print_notices($location='general'){
		if(isset($this->notices[$location])){
			$notices=$this->notices[$location];
			if(isset($notices["error"])){
				foreach($notices["error"] as $notice){
					?>
					<div class="buttetini_notice buttetini_err"><?php echo $notice?></div>
					<?php
				}
			}
			if(isset($notices["message"])){
				foreach($notices["message"] as $notice){
					?>
					<div class="buttetini_notice buttetini_msg"><?php echo $notice?></div>
					<?php
				}
			}
			if(isset($notices["information"])){
				foreach($notices["information"] as $notice){
					?>
					<div class="buttetini_notice buttetini_info"><?php echo $notice?></div>
					<?php
				}
			}
		}
	}
	public function option($option_name, $apply_the_content=false, $default_value=""){
		$value=get_option("bulletini_".$option_name, "");
		if($value==""){
			$value=$default_value;
		}
		if($apply_the_content){
			$value=apply_filters('the_content', $value);
		}
		return $value;
	}
}